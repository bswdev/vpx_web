/**
 * search
 */
$(document).ready(function() {
	// cuisines
	var $cuisine1 = $("#cuisine1")
	var $cuisine2 = $("#cuisine2")
	var $cuisine3 = $("#cuisine3")
	var $add = $("#add")
	
	// location
    var geocoder = new google.maps.Geocoder()
    var $address = $("#address")
    var $latitude = $("#latitude")
    var $longitude = $("#longitude")
    var geocoded = false
    
    // search
    var $search = $("#search")
    var $start = $("#start")
	
	// focus
	//$('form:first *:input[type!=hidden]:first').focus()

	// hint stuff
	$("input[title]").each(function() {
		var $this = $(this)

		// blur
		$this.blur(function() {
			if (!$this.val())
				$this.val($this.attr("title")).addClass("hint")
		})
		
		// focus
		$this.focus(function() {
			if ($this.val() == $this.attr("title"))
				$this.val("").removeClass("hint")
		})

		// initialize
		if (!$this.val())
			// no value, set hint
			$this.val($this.attr("title")).addClass("hint")
		else if ($this.val() == $this.attr("title"))
			// hint set, add class
			$this.addClass("hint")
		else
			// value exists
			$this.removeClass("hint")
	})

	// enter
	$(document).keypress(function(event) {
		var kc = (event.keyCode ? event.keyCode : event.which)
		if (kc == 13)
			//search("300")
			$search.click()
	})
	
	// address
//	var timer = 0
//	
//	$address.on("keyup change paste", function() {
//		if (timer != null) {
//			clearTimeout(timer)
//			timer = setTimeout(function() {
//				if ($.trim($address.val()) == '' || $address.attr("title") == $address.val()) {
//					$("#distance").attr("disabled", "disabled")
//					//$("#distance option").eq(0).attr("selected", true)
//				} else
//					$("#distance").removeAttr("disabled")
//			}, 500)
//		} 
//	}).trigger("change")	
	
    $cuisine1.change(function () {
        if ($cuisine1.val() == ''){
        	// suppress cuisine2
        	$cuisine2.hide()
        	$cuisine2.find("option").eq(0).attr('selected', true)
        	
        	// suppress cuisine3
        	$cuisine3.hide()
        	$cuisine3.find("option").eq(0).attr('selected', true)
        	
        	$add.hide()
        } else
        	$add.show()
    }).change()
    
    $cuisine2.change(function () {
    	onCuisine($(this))
    }).change()
    
    $cuisine3.change(function () {
    	onCuisine($(this))
    }).change()
    
    function onCuisine(selection) {
        selection.each(function () {
            if ($(this).val() == ''){
            	selection.hide()
            	$add.show()
            }
          });
    }
	
    // show add?
    if ($("select:hidden").length > 0 && $cuisine1.val() != '')
    	$add.show()
    else
    	$add.hide()
    	
    // add
    $add.click(function () {
    	switch ($("select:hidden").length) {
    	case 0:
    		// ni
    		break
    	case 1:
    		// hide button
    		$add.hide()
    	default:
    		// add cuisine
            var cuisine = $("select:hidden").eq(0)
            cuisine.fadeIn(500)
            
            // select second item
            if (cuisine != $("#cuisine1"))
            	cuisine.find("option").eq(1).attr('selected', true)
            break
    	}
    })
    	
	// search
//	$search.click(function(){
		// reset geocoding
//		geocoded = false
//	})
    
	// more
    $('#more').click(function() {
    	// set offset
    	$start.val($(this).attr("value"))
    	
    	// already geocoded
    	geocoded = true
    	
    	// execute search
    	$search.click()
    })
    
//    function search(s) {
//    	// assign start
//    	start = s
//    	
//    	// geocoding false
//    	geocoded = false
//
//    	// submit form
//    	$search.click()
//    }
    
    $("#vp").submit(function() {
    	// clear all hints
    	$("input.hint").val("")
    	
    	// ready for search
    	if (geocoded) {
    		geocoded = false
    		
    		// perform search
    		return true
    	}
    	
		// perform geocode
        var address = $.trim($address.val())//form.elements[4].value
        
        if (address) {
            geocoder.geocode({
                "address" : address
            }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    // assign coordinates
                    var point = results[0].geometry.location
                    $latitude.val(point.lat())
                    $longitude.val(point.lng())

                    // search
                    geocoded = true
                    //$("#vp").submit()
                    $search.click()
                } else {
                    // no resolution
                    alert(address + " not found, please try another address")
                    geocoded = false
                }
            })
            
            // suppress search
            return false
        } else {
            // no address provided
            //document.getElementById("search").click()
            $latitude.val("0")
            $longitude.val("0")
            
            // perform search
        	return true
        }
    })
});