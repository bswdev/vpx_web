/**
 * init style, replace origin with http://www.vittlepix.com
 */
window.onload = function() {
	var origin = "http://www.vittlepix.com";
	window.addEventListener(
		"message",
		function receive(event) {
			// external
			style = document.getElementsByTagName("link")[0];
			if (style != null)
				window.frames[0].postMessage(style.href, origin);
		
			// internal
			var style = document.getElementsByTagName("style")[0];
			if (style != null)
				window.frames[0].postMessage(style.textContent, origin);
		},
		false);
	
	/*
	function receive(event) {
		// external
		style = document.getElementsByTagName("link")[0];
		if (style != null)
			window.frames[0].postMessage(style.href, "http://www.vittlepix.com");
		
		// internal
		var style = document.getElementsByTagName("style")[0];
		if (style != null)
			window.frames[0].postMessage(style.textContent, "http://www.vittlepix.com");
	}
	
	window.addEventListener("message", receive, false);
	*/
}