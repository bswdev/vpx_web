<%@ page contentType="text/html; charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%
// send to search if no id specified
if (request.getParameter("id") == null)
	response.sendRedirect("/search.htm");
%>
<!doctype html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>vittlepix - menu</title>

<!-- google maps -->
<script type="text/javascript"
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCn8dEEqu9AYI33NBCmx8ndOsAex3wRXSo&sensor=false"></script>

<!-- jquery -->
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>

<!-- style -->
<script type="text/javascript">
	window.onload = function() {
		// mobile detection
	    if ($("#menu").css("display") == "none")
	    	   window.location.assign("http://m.vittlepix.com/m?id=" + ${param.id});
		
		// create style listener and attach
		window.addEventListener(
			"message",
		     function receive(event) {
	            // style element
	            var e;
	            
	            if (event.data.search("http") == -1) {
	                // create internal style
	                e = document.createElement("style");
	                e.type = "text/css";
	                e.textContent = event.data;
	            } else {
	                // create external style
	                e = document.createElement("link");
	                e.type = "text/css";
	                e.rel = "stylesheet";
	                e.href = event.data;
	            }

	            // append to head
	            document.getElementsByTagName("head")[0].appendChild(e);
	        }, false);
		
		// notify parent ready to receive style (does not work in some IE)
		parent.postMessage("", "*");
	}
</script>

<!-- menu -->
<script type="text/javascript" language="javascript" src="/mnu/mnu.nocache.js"></script>
</head>
<body>
	<div id="menu"
	    kitchen="${param.id}"
	    columns="${empty param.cols ? 1 : param.cols}"
	    embed="${empty param.embed ? 0 : param.embed}"
	    ${param.embed == 1 ? "" : "style='margin: 0 auto; width: 850px;'"}>
	    
	    <c:if test="${!empty param.id}">
	        <a href="javascript:window.history.back()">&lt; back to results</a>
	    </c:if>
	</div>
</body>
</html>