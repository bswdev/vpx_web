<%@ page contentType="text/html; charset=UTF-8" language="java"
	isELIgnored="false" session="false"%>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<title>vittlepix - visual menu hosting</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<!--
        The second stylesheet is to make things look pretty.
        The first one is only the important one.
        -->
<link rel="stylesheet" type="text/css" media="screen"
	href="css/layout.css" />
<link rel="stylesheet" type="text/css" media="screen"
	href="css/style.css" />

<!-- jquery -->
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>

<!-- maps api load -->
<script type="text/javascript"
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCn8dEEqu9AYI33NBCmx8ndOsAex3wRXSo&sensor=false"></script>

<!-- search -->
<script type="text/javascript" src="js/search.js"></script>
</head>
<body>
	<!-- content -->
	<div class="wrapper">
		
		<!-- header -->
		<div class="header">
			<div style="float: left; height: 90px; width: 120px;">
				<img src="/favicon.ico" style="height: 90px; width: 120px; display: none;"/>
			</div>

			<div style="float: right; width: 120px; text-align: right;">
			    <a href="/faq.html">faq</a> |
				<a href="<%=UserServiceFactory.getUserService().createLoginURL("https://vittlepix.appspot.com")%>">login</a>
			</div>

			<s:form action="/search.htm" method="post" id="vp">
				<table id="query" cellpadding="0" cellspacing="0">
					<tr>
						<td class="label">location</td>
						<td>
							<div>
								<s:text name="address" id="address" title="full address, city & state or postal code" />

								&nbsp;within
								<s:select name="distance" id="distance">
									<s:options-collection
										collection="${actionBean.context.distances}"
										style="text-align: left" />
								</s:select>
								miles
							</div>
						</td>
					</tr>
					<tr>
						<td class="label">cuisines</td>
						<td>
							<div style="text-align: left">
								<!-- cuisines -->
								<s:select name="cuisine1" id="cuisine1">
									<s:option value="">any</s:option>
									<s:options-collection
										collection="${actionBean.context.cuisines}" label="name"
										value="id" />
								</s:select>

								<s:select name="cuisine2" id="cuisine2">
									<s:option value="">none</s:option>
									<s:options-collection
										collection="${actionBean.context.cuisines}" label="name"
										value="id" />
								</s:select>

								<s:select name="cuisine3" id="cuisine3">
									<s:option value="">none</s:option>
									<s:options-collection
										collection="${actionBean.context.cuisines}" label="name"
										value="id" />
								</s:select>

								<!-- add
					                <input type="button" value="+ cuisine" id="add" />
					                -->
								<a class="button" href="javascript:;" id="add">add
									cuisine</a>
							</div>
						</td>
					</tr>
					<tr>
						<td class="label">name</td>
						<td>
							<div>
								<s:text name="name" id="name" title="first letter(s), word(s) or complete name" />							     
							    <s:submit name="search" id="search" value="search" class="button" style="width: 58px;" />
							</div>
						</td>
					</tr>
				</table>

				<s:hidden name="latitude" id="latitude" />
				<s:hidden name="longitude" id="longitude" />
				<s:hidden name="start" id="start" />
			</s:form>
		</div>

		<table id="results" cellpadding="5" cellspacing="0">
			<c:choose>
				<c:when test="${empty actionBean.result}">
				    <c:choose>
					    <c:when test="${empty actionBean.message}" />
					    <c:otherwise>
							<tr>
								<td style="text-align: center;">${actionBean.message}</td>
							</tr>
						</c:otherwise>
					</c:choose>
				</c:when>
				<c:otherwise>
					<c:forEach items="${actionBean.result}" var="kitchen">
						<tr>
							<td class="name">${kitchen.name}</td>
							<c:if test="${actionBean.measured}">
								<td class="distance">${kitchen.distance} miles</td>
							</c:if>
							<td class="desc"><i>${kitchen.tags}</i><br/> ${empty kitchen.desc
								? "" : kitchen.desc}</td>
							<td class="menu"><a class="button"
								href="/m?id=${kitchen.id}&cols=1"
								style="width: 100px;">menu</a>
							</td>
						</tr>
					</c:forEach>
				</c:otherwise>
			</c:choose>
		</table>

        <c:if test="${actionBean.start > 0}">
        <!-- more -->
        <div style="width: 98%; text-align: right; padding: 10px 10px 0px 0px; margin: 0 auto;">
            <input type="button" class="button" id="more" value="${actionBean.start}" style="width: 100px;" />
        </div>
        </c:if>
		<div class="push"></div>
	</div>

	<!-- footer -->
	<div class="footer">
		<p>  
			vittlepix visual menu hosting &mdash; Copyright &copy; 2012 boosetec &mdash; <a
				href="http://boosetec.com/" title="boosetec">boosetec.com</a>
		</p>
	</div>
</body>
</html>