<%@ page contentType="text/html; charset=UTF-8" language="java" isELIgnored="false" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<!doctype html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>vittlepix - mobile</title>

<!-- google maps -->
<script type="text/javascript"
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCn8dEEqu9AYI33NBCmx8ndOsAex3wRXSo&sensor=false"></script>
</head>

<!-- gwt -->
<meta name='gwt:property' content='locale=en_US'>
<script type="text/javascript" language="javascript" src="/mob/mob.nocache.js"></script>
<%
  //out.println("user agent: " + request.getHeader("User-Agent"));
%>
<body>
    <div id="mobile" kitchen="${param.id}"></div>
    
	<!-- history -->
	<iframe src="javascript:''" id="__gwt_historyFrame" tabIndex='-1'
		style="position: absolute; width: 0; height: 0; border: 0"></iframe>

	<!-- js warning -->
	<noscript>
		<div
			style="width: 22em; position: absolute; left: 50%; margin-left: -11em; color: red; background-color: white; border: 1px solid red; padding: 4px; font-family: sans-serif">
			Your web browser must have JavaScript enabled in order for this
			application to display correctly.</div>
	</noscript>
</body>
</html>