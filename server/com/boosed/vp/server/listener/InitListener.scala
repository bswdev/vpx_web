package com.boosed.vp.server.listener

import com.google.inject.Injector
import com.google.inject.servlet.GuiceServletContextListener
import com.google.inject.Guice
import com.boosed.vp.server.conf.ServerModule
import com.boosed.vp.server.conf.ServletModule
import com.google.inject.Stage
import com.google.inject.Module
import scala.collection.JavaConversions._
import javax.servlet.ServletContextEvent

class InitListener extends GuiceServletContextListener {

  override def contextInitialized(sce: ServletContextEvent) = {
    super.contextInitialized(sce)

    // prevents breakage of rest services
    System.setProperty("javax.xml.transform.TransformerFactory",
      "com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl")
  }
  
  override protected def getInjector: Injector = Option(InitListener.injector).toLeft(()).fold(
    // return injector
    identity, //i => i,

    // create injector
    _ => {
      InitListener.injector = Guice.createInjector(Stage.PRODUCTION,
        List(
          // add modules here
          new ServerModule,
          new ServletModule))
      InitListener.injector
    })
}

object InitListener {
  var injector: Injector = _
}