package com.boosed.vp.server.extension

import com.boosed.vp.shared.db.Cuisine
import java.util.List
import net.sourceforge.stripes.action.ActionBeanContext
import com.boosed.vp.server.listener.InitListener
import com.boosed.vp.server.service.CuisineService
import com.boosed.orm.gae.dao.crit.SortCriterion
import scala.collection.JavaConversions._
import com.googlecode.objectify.Key

class SearchContext extends ActionBeanContext {
  def getCuisines = Option(SearchContext.cuisines) match {
    case Some(x) => x
    case None =>
      SearchContext.cuisines = InitListener.injector.getInstance(classOf[CuisineService])
        .get(SortCriterion("name"))(_ => true)._1
      SearchContext.cuisines
  }

  def getDistances = SearchContext.distances
}

object SearchContext {

  var cuisines: List[Cuisine] = _
  val distances = Array(1, 5, 10, 25, 50, 100, 150)
}