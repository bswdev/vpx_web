package com.boosed.vp.server.servlet

import com.google.inject.Inject
import com.google.inject.Singleton
import com.boosed.orm.gae.dao.crit.SortCriterion
import com.boosed.orm.gae.traits.Loggable
import com.boosed.vp.server.service._
import com.boosed.vp.server.traits._
import com.boosed.vp.shared.db._
import com.boosed.vp.shared.db.embed.Hours
import com.boosed.vp.shared.db.enums._
import com.boosed.vp.shared.exception._
import com.boosed.vp.shared.rpc.RpcService
import com.googlecode.objectify.Key
import com.google.appengine.api.blobstore.BlobstoreService
import com.google.gwt.user.server.rpc.RemoteServiceServlet
import java.util.ArrayList
import java.util.List
import scala.collection.JavaConversions._

@Singleton
class RpcServlet extends RemoteServiceServlet
  with RpcService
  with Authorizable
  with Loggable {

  // passthru for all signatures requiring a filter
  private implicit def passthru(a: AnyRef) = true

  override def createLoginUrl(url: String): String = userService.createLoginURL(url)

  override def createUploadUrl(url: String): String = {
    // ensure user is enabled
    enabled

    // return url
    blobstoreService.createUploadUrl('/' +: url)
  }

  override def initialize: String = if (userService.isUserLoggedIn) {
    val user = userService.getCurrentUser
    val id = user.getUserId

    // some quick tests
    //    val c = kitchenService.getCategories(4).head
    //    dishService.save(Option(new Dish), scala.collection.mutable.Map(
    //        "kitchen" -> "4",
    //        "name" -> "testit",
    //        "desc" -> "again",
    //        "each" -> "7.35",
    //        "for two" -> "14.00",
    //        c.id.toString -> ""))

    // more monadic
    accountService.get(id).getOrElse {
      accountService.save(new Account(id, user.getNickname, user.getEmail)).head._2
    }.id

    //    accountService.get(id).toLeft(new Account(id, user.getNickname, user.getEmail)).fold(
    //      // return id
    //      _.id,
    //      // create account & return id
    //      accountService.save(_).head._1.getName)
  } else null // user not logged in, return null

  override def loadAccount: Account = accountService.get(authenticated.getName).getOrElse(throw new NotFoundException)

  override def loadComment(comment: String Tuple java.lang.Long): Comment = {
    // render the appropriate key
    val key = comment.a match {
      case "dish" => comment.b.longValue: Key[Dish]
      case "kitchen" => comment.b.longValue: Key[Kitchen]
    }

    // retrieve the comment (if doesn't exist, use current user as author)
    commentService.get(key).getOrElse(new Comment(key, implicitly))
  }

  override def loadCuisines: List[Cuisine] = new ArrayList(cuisineService.get(SortCriterion("name"))._1)

  // limited to owners
  //override def loadCategory(category: Long): Category = categoryService.getVerified(category)//(categoryService.get(category))

  // include "uncategorized" if user is owner
  override def loadCategories(kitchen: Long, isMenu: Boolean): List[Category] =
    new ArrayList(kitchenService.getCategories(kitchen)(if (isMenu) null else accounted))

  // limited to owners
  //override def loadDish(dish: Long): Dish = dishService getVerified dish//(dishService.get(dish))
  //dishService.get(dish).toRight(new NotFoundException).fold(throw _, d => d)

  // load all if not menu AND user == owner
  override def loadDishes(category: Long, isMenu: Boolean): List[Dish] =
    new ArrayList(categoryService.getDishes(category)(if (isMenu) null else accounted))

  // kitchen loads for menu
  override def loadKitchen(kitchen: Long): Kitchen =
    //kitchenService.getVerified(kitchenService.get(kitchen))
    kitchenService get kitchen getOrElse (throw new NotFoundException)

  // limited to owners
  override def loadKitchens(t: TagType, cursor: String): List[Kitchen] Tuple String = {
    val (results, newCursor) = tagService.verified(t, cursor)
    // serializable rpc Tuple NOT Tuple2/Pair
    new Tuple(new ArrayList(results), newCursor)
  }

  // somehow this uses the implicit argument instead of the base def without
  override def remove(kind: String, id: Long) = kind match {
    case "dish" => dishService.drop(id)(enabled)
    case "category" => categoryService.drop(id)(enabled)
    case "kitchen" => kitchenService.drop(id)(enabled)
    case _ => throw new UnknownEntityException
  }

  override def schedule(kitchen: Long, schedule: java.util.Map[Day, Hours]) {
    // get verified
    val k = kitchenService.verified(kitchen)(enabled)

    // assign schedule and save
    k.schedule = schedule
    kitchenService.save(k)
  }

  override def search(cuisines: List[java.lang.Long], search: String, location: List[java.lang.Double], offset: Int, limit: Int): List[Kitchen] =
    // wrap in ArrayList for GWT-RPC compatibility
    //new ArrayList(kitchenService.locate(Pair(location(0), location(1)), location(3).doubleValue())._1)
    new ArrayList(kitchenService.search(cuisines map (_.longValue) toList, search, location map (_.doubleValue) toList, offset)._1)

  override def store(values: java.util.Map[String, String]) {
    // 1) check if user account exists
    // 2) get kind and check if user owns that kind
    // 3) update the entity

    // update or store new entity
    val id = Option(values.remove("id"))

    // match by kinds
    Option(values.remove("kind")) match {
      // no kind provided
      case None => throw new UnknownEntityException
      case Some(kind) => kind match {
        //        case "dish" => id.toRight(Option(new Dish)).fold(
        //          // create new
        //          dishService.save(_, values),
        //          // update existing
        //          id => dishService.save(dishService.get(id), values))
        case "category" => id.toRight(Option(new Category)).fold(
          categoryService.save(_, values)(enabled),
          id => categoryService.save(categoryService.get(id), values)(enabled))
        case "kitchen" => id.toRight(Option(new Kitchen)).fold(
          kitchenService.save(_, values)(enabled),
          id => kitchenService.save(kitchenService.get(id), values)(enabled))
        case "cuisine" => Option(values.remove("name")) match {
          case Some(name) => cuisineService.save(name)
          case None => throw new EntityUpdateException
        }
        case "account" => accountService.save(values)(enabled)
        case _ => throw new UnknownEntityException
      }
    }
  }

  override def toggle(values: String Tuple String): Boolean = Option(values.a) match {
    // no kind provided
    case None => throw new UnknownEntityException
    case Some(kind) => kind match {
      // verify against owning user
      case "dish" => (dishService.toggle(values.b)(enabled)).visible
      // verify against admin
      case "account" => accountService.toggle(values.b).enabled
      case _ => throw new UnknownEntityException
    }
  }

  // injections
  @Inject var blobstoreService: BlobstoreService = _
  @Inject var categoryService: CategoryService = _
  @Inject var commentService: CommentService = _
  @Inject var cuisineService: CuisineService = _
  @Inject var dishService: DishService = _
  @Inject var kitchenService: KitchenService = _
  @Inject var tagService: TagService = _

  // put test crap in here
  def test(values: java.util.Map[String, String]) {
    // update or store new entity
    val id = Option(values.remove("id"))

    accountService.toggle("adfadfa")
    dishService.toggle("45")(enabled)
    // id
    //commentService.get()

    // get all the dishes of a kitchen
    //  {
    //    val cats = categoryService.get(EQCriterion("kitchen", new Key(classOf[Kitchen], kitchen)))._1
    //    if (cats.isEmpty)
    //      new ArrayList[Dish]
    //    else
    //      new ArrayList(dishService.get(
    //        InCriterion("categories", cats.map(c => new Key(classOf[Category], c.id.longValue))))._1)
    //  }

    //  def filterFeed(feed: Elem, feedId: String): Seq[Node] = {
    //    var results = new Queue[Node]()
    //    feed \ "entry" foreach { (entry) =>
    //      if (search(entry \ "service" \ "id" last, feedId)) {
    //        results += (entry \ "user" \ "nickname").last
    //      }
    //    }
    //    return results
    //  }
    //
    //  def search(p: Node, Name: String): Boolean = p match {
    //    case <id>{ Text(Name) }</id> => true
    //    case _ => false
    //  }
  }

  //  def monadstuff {
  //    val g = scala.collection.immutable.List(1, 2, 3).flatMap(kitchenService.get(_).get.cuisines)
  //  }
}