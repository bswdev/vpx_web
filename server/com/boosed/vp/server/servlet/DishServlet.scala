package com.boosed.vp.server.servlet

import com.google.inject.Inject
import com.google.inject.Singleton
import com.google.inject.name.Named
import com.boosed.vp.server.service.DishService
import com.boosed.vp.server.traits._
import com.boosed.vp.shared.db.Dish
import com.boosed.vp.shared.db.embed.Image
import com.boosed.vp.shared.exception._
import javax.servlet.http.HttpServlet
import scala.collection.JavaConversions._
import com.boosed.orm.gae.traits.Loggable
import com.googlecode.objectify.Key
import com.boosed.vp.shared.db.Account
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

// cropped dimensions of food image
@Singleton
class DishServlet @Inject() (
  @Named("image_width") width: Int,
  @Named("image_height") height: Int)
  extends HttpServlet
  with Authorizable
  with Imageable[Dish]
  with Loggable {

  override def doGet(req: HttpServletRequest, res: HttpServletResponse) {
    res.setHeader("Content-Type", "text/html")

    // this is not working for some reason:
    // The result html can be null as a result of submitting a form to a different domain.
    val writer = res.getWriter
    writer.println(req.getParameter("url"))
    writer.flush
    writer.close
  }

  override def doPost(req: HttpServletRequest, res: HttpServletResponse) {
    // set parameter encoding (does logging lock in the encoding?)
    req.setCharacterEncoding("UTF-8")

    // make sure account is authorized
    val authorized = enabled

    // handle image if present
    var key = getImage(req)

    try {
      // turn parameter map to name-value pairs
      val values = req.getParameterMap.flatMap(e => Map(e._1.asInstanceOf[String] -> e._2.asInstanceOf[Array[String]].head))

      val dish = values.remove("id").getOrElse(throw new InvalidIdException).trim match {
        // create dish
        case "" => dishService.save(Option(new Dish), values)(authorized)
        // update dish
        case id => dishService.save(dishService.get(id), values)(authorized)
      }

      //    val dish = SS.dishService.getVerified(values.remove("id").getOrElse(None))
      //    
      //    values.remove("id").get
      //    // create/get the dish
      //    val dish = values.remove("id").toRight(Option(new Dish)).fold(
      //      // create new
      //      SS.dishService.save(_, values),
      //      // update existing
      //      id => SS.dishService.save(SS.dishService.get(id), values))

      //    // check user is logged in
      //    val account = ensureAccount

      //Option(SS.blobstoreService.getUploads(req).get("image").head).toRight(None).fold(
      // no image
      //  _ => res.sendRedirect("/dish?url=noimage"),

      // found blobkey
      //  key => {
      //        Option(req.getParameter("dish")).toRight(None).fold(
      //        // no dish parameter
      //        _ => {
      //          // delete uploaded image
      //          ServiceServlet.blobstoreService.delete(key)
      //          res.sendRedirect("/upload?url=nodish")
      //        },
      //
      //        // add image to dish
      //        dish => ServiceServlet.dishService.get(dish) match {
      //          // dish found, process image
      //          case Some(dish) =>

      //        // apply resize transform (square) to image
      //        var image = SS.imagesService.applyTransform(
      //          ImagesServiceFactory.makeResize(width, width), ImagesServiceFactory.makeImageFromBlob(key), OutputEncoding.JPEG)
      //
      //        // height exceeds limit, perform crop
      //        val ih = image.getHeight
      //        val hdelta: Double = (ih - height) / (2.0 * ih)
      //        if (hdelta > 0)
      //          image = SS.imagesService.applyTransform(
      //            ImagesServiceFactory.makeCrop(0, hdelta, 1, 1 - hdelta), image, OutputEncoding.JPEG)

      // resize image to desired dimensions
      //val image = imagesService.applyTransform(
      //  ImagesServiceFactory.makeResize(width, height, 0.5, 0.5), ImagesServiceFactory.makeImageFromBlob(key), OutputEncoding.JPEG)

      //          // if height exceeds 240, crop the top and bottom
      //          val ih = image.getHeight
      //          if (ih > height) {
      //            var delta: Double = ih - height
      //            delta /= (2.0 * ih)
      //            val crop = ImagesServiceFactory.makeCrop(0, delta, 1, 1 - delta);
      //            image = ServiceServlet.imagesService.applyTransform(crop, image, OutputEncoding.JPEG);
      //          }

      // delete the old blob
      //blobstoreService.delete(key)

      // create a new Blob file with mime-type "image/jpg"
      //val file = SS.fileService.createNewBlobFile("image/jpg");

      // open a channel to write to it and finalize
      //val channel = SS.fileService.openWriteChannel(file, true)
      //channel.write(ByteBuffer.wrap(image.getImageData))
      //channel.closeFinally

      // now read from the file using the Blobstore API
      val (blobKey, url) = resizeImage(key, width, height)
      key = blobKey //SS.fileService.getBlobKey(file)

      // add image to dish (user-submitted or owner generated)
      //val url = imagesService.getServingUrl(key)
      addImage(dish, new Image( /*key.getKeyString*/ key, url))(authorized)

      // send back to the get handler of this servlet
      res.sendRedirect("/dish?url=" concat url)
    } catch {
      case iae: IllegalArgumentException =>
        // no image was provided to store
        warning("no dish picture to upload, ignored")
        
        // "delete" image
        dropImage(key)
        
        // redirect with no error
        res.sendRedirect("dish?url=noimage")
      case e =>
        // ApiDeadlineExceededException e:
        // image uploaded but url was not generated
        // The API call images.GetUrlBase() took too long to respond and was cancelled.
        warning("failed to edit dish: " + e.getClass)

        // delete image
        dropImage(key)

        // send back to the get handler of this servlet
        res.sendRedirect("/dish?url=" concat e.getClass.getSimpleName)
    }

    //          // dish not found
    //          case None =>
    //            // delete uploaded image
    //            ServiceServlet.blobstoreService.delete(key)
    //            res.sendRedirect("/upload?url=notfound")
    //     })
  }

  override def addImage(dish: Dish, image: Image)(implicit user: Key[Account]) =
    // delegate to dish service
    dishService.addImage(dish, image)(user)

  // injections
  @Inject var dishService: DishService = _
}