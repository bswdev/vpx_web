package com.boosed.vp.server.servlet

import com.google.inject.Inject
import com.google.inject.Singleton
import com.google.inject.name.Named
import com.boosed.vp.server.service._
import com.boosed.vp.server.traits._
import com.boosed.vp.shared.db._
import com.boosed.vp.shared.db.embed.Image
import com.boosed.vp.shared.exception._
import javax.servlet.http.HttpServlet
import scala.collection.JavaConversions._
import com.boosed.orm.gae.traits.Loggable
import com.googlecode.objectify.Key
import com.boosed.vp.shared.db.Account
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

// cropped dimensions of food image
@Singleton
class CommentServlet @Inject() (
  @Named("image_width") width: Int,
  @Named("image_height") height: Int)
  extends HttpServlet
  with Authorizable
  with Imageable[Comment]
  with Loggable {

  override def doGet(req: HttpServletRequest, res: HttpServletResponse) {
    res.setHeader("Content-Type", "text/html")

    // this is not working for some reason:
    // The result html can be null as a result of submitting a form to a different domain.
    val writer = res.getWriter
    writer.println(req.getParameter("url"))
    writer.flush
    writer.close
  }

  override def doPost(req: HttpServletRequest, res: HttpServletResponse) {
    // set parameter encoding (does logging lock in the encoding?)
    req.setCharacterEncoding("UTF-8")

    // make sure account is authorization
    val authorized = enabled

    // handle image if present
    var key = getImage(req)

    // turn parameter map to name-value pairs
    val values = req.getParameterMap.flatMap(e => Map(e._1.asInstanceOf[String] -> e._2.asInstanceOf[Array[String]].head))

    val comment = {
      try
        Right(values.remove("dish").getOrElse(throw new InvalidIdException).toLong)
      catch {
        case _ => Left(values.remove("kitchen").getOrElse(throw new InvalidIdException).toLong)
      }
    } match {
      // dish exists?
      case Right(d) => dishService.comment(d, values)(authorized)
      // kitchen exists?
      case Left(k) => kitchenService.comment(k, values)(authorized)
    }

    try {
      // create image with new dimensions
      val (blobKey, url) = resizeImage(key, width, height)
      key = blobKey

      // add image to comment (user-submitted or owner generated)
      addImage(comment, new Image(key, url))(authorized)

      // send back to the get handler of this servlet
      res.sendRedirect("/comment?url=" concat url)
    } catch {
      case iae: IllegalArgumentException =>
        // no image was provided to store
        warning("no comment picture to upload, ignored")

        // "delete" image
        dropImage(key)
        
        // is the comment also empty?
        if (comment.comment.isEmpty && !comment.recommend) {
          warning("no comment data, removing")
          commentService.drop(comment)
        }

        // redirect with no error
        res.sendRedirect("comment?url=noimage")
      case e =>
        // ApiDeadlineExceededException e:
        // image uploaded but url was not generated
        // The API call images.GetUrlBase() took too long to respond and was cancelled.
        warning("failed to comment: " + e.getClass)

        // delete image
        dropImage(key)

        // send back to the get handler of this servlet
        res.sendRedirect("/comment?url=" concat e.getClass.getSimpleName)
    }
  }

  override def addImage(comment: Comment, image: Image)(implicit user: Key[Account]) =
    commentService.addImage(comment, image)(user)

  // injections
  @Inject var commentService: CommentService = _
  @Inject var dishService: DishService = _
  @Inject var kitchenService: KitchenService = _
}