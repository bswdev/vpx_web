package com.boosed.vp.server.servlet

import com.google.inject.Inject
import com.google.inject.Singleton
import com.google.inject.name.Named
import com.boosed.vp.shared.db.Kitchen
import com.boosed.vp.shared.db.embed.Image
import com.boosed.vp.shared.exception._
import scala.collection.JavaConversions._
import com.boosed.vp.server.traits._
import javax.servlet.http.HttpServlet
import com.boosed.vp.server.service.KitchenService
import com.boosed.orm.gae.traits.Loggable
import com.googlecode.objectify.Key
import com.boosed.vp.shared.db.Account
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Singleton
class ImageServlet @Inject() (
  @Named("image_width") width: Int,
  @Named("image_height") height: Int)
  extends HttpServlet
  with Authorizable
  with Imageable[Kitchen]
  with Loggable {

  override def doGet(req: HttpServletRequest, res: HttpServletResponse) {
    res.setHeader("Content-Type", "text/html")

    // this is not working for some reason:
    // The result html can be null as a result of submitting a form to a different domain.
    val writer = res.getWriter
    writer.println(req.getParameter("url"))
    writer.flush
    writer.close
  }

  override def doPost(req: HttpServletRequest, res: HttpServletResponse) {
    // set parameter encoding (does logging lock in the encoding?)
    req.setCharacterEncoding("UTF-8")

    // make sure account is authorization
    val authorized = enabled
    
    // handle image if present (will throw error if image is not present)
    var key = getImage(req) //blobstoreService.getUploads(req).get("image").head

    try {
      // turn parameter map to name-value pairs
      val values = req.getParameterMap.flatMap(e => Map(e._1.asInstanceOf[String] -> e._2.asInstanceOf[Array[String]].head))

      values.remove("id").getOrElse(throw new InvalidIdException).trim match {
        // no id
        case "" => throw new NotFoundException

        // retrieve kitchen
        case id =>
          val kitchen = kitchenService.verified(id)(authorized)

          // create image with new dimensions
          val (blobKey, url) = resizeImage(key, width, height)
          key = blobKey

          // add image to dish (user-submitted or owner generated)
          addImage(kitchen, new Image(key, url))(authorized)

          // send back to the get handler of this servlet
          res.sendRedirect("/image?url=" concat url)
      }
    } catch {
      case iae: IllegalArgumentException =>
        // no image was provided to store
        warning("no kitchen picture to upload, ignored")
        
        // "delete" image
        dropImage(key)
        
        // redirect with no error
        res.sendRedirect("image?url=noimage")
      case e =>
        // ApiDeadlineExceededException e:
        // image uploaded but url was not generated
        // The API call images.GetUrlBase() took too long to respond and was cancelled.
        warning("failed to edit kitchen " + e.getClass)
        
        // delete image
        dropImage(key)

        // send back to the get handler of this servlet
        res.sendRedirect("/image?url=" concat e.getClass.getSimpleName)
    }
  }

  override def addImage(kitchen: Kitchen, image: Image)(implicit user: Key[Account]) =
    // delegate to kitchen service
    kitchenService.addImage(kitchen, image)(user)
    
  // injections
  @Inject var kitchenService: KitchenService = _
}