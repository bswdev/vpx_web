package com.boosed.vp.server.action;

import com.boosed.vp.server.service.KitchenService
import com.boosed.orm.gae.traits.Loggable
import com.boosed.vp.server.extension.SearchContext
import com.boosed.vp.server.listener.InitListener
import com.boosed.vp.shared.db.Kitchen
import java.util.ArrayList
import net.sourceforge.stripes.action._
import scala.annotation.tailrec
import scala.annotation.target.field
import scala.reflect.BeanProperty
import scala.collection.JavaConversions._

@UrlBinding(value = "/search.htm")
class SearchAction extends ActionBean with Loggable {

  /** maximum number of results per lookup */
  val limit = 25

  var context: SearchContext = _

  @BeanProperty var start: Int = 0
  @BeanProperty var cuisine1: Long = _
  @BeanProperty var cuisine2: Long = _
  @BeanProperty var cuisine3: Long = _
  @BeanProperty var name: String = _
  @BeanProperty var distance: Int = _
  @BeanProperty var message: String = _
  @BeanProperty var result: java.util.List[Kitchen] = _
  @BeanProperty var measured: Boolean = _
  var latitude: Double = _
  var longitude: Double = _

  override def getContext: SearchContext = context
  override def setContext(context: ActionBeanContext) = this.context = context.asInstanceOf[SearchContext]

  def getLatitude = latitude
  def setLatitude(value: Double) = {
    latitude = value
    setMeasured(latitude != 0d)
  }

  def getLongitude = longitude
  def setLongitude(value: Double) = {
    longitude = value
    setMeasured(longitude != 0d)
  }

  //def isMeasured = measured
  //  def setResult(value: Long) = result = value
  //	public void setContext(ActionBeanContext context) {
  //		this.context = (SearchContext) context;
  //		//System.out.println(context.hashCode() + "");
  //		this.context.setCuisines(InitListener.injector().getInstance(CuisineService.class).getAll());
  //		//context.getRequest().setAttribute("cuisines", InitListener.injector().getInstance(CuisineService.class).getAll());
  //	}
  //
  //	public List<Cuisine> getCuisines() {
  //		System.out.println("fetching cuisines");
  //		return InitListener.injector().getInstance(CuisineService.class).getAll();
  //	}

  // landing handler
  @DefaultHandler
  def default: Resolution = {
    // assign empty results
    setMessage("") //"please enter your search criteria"
    setResult(new ArrayList)
    new ForwardResolution("/WEB-INF/jsp/search.jsp")
  }

  // search handler
  @HandlesEvent("search")
  def search: Resolution = {
    // prepare cuisines
    val cuisines = List(cuisine1, cuisine2, cuisine3).filterNot(0==)
    //    .flatMap(_ match {
    //      case a: Long if (a > 0) => List(a)
    //      // no cuisine
    //      case b => Nil
    //    })

    // prepare location
    val location = List(latitude, longitude).filterNot(0==) match {
      case Nil => Nil
      case x => distance.toDouble :: x
    }
    
    //    flatMap(_ match {
    //      case a: Double if (a != 0) => List(a)
    //      // non-zero coordinate
    //      case b => Nil
    //    })

    val kserv = InitListener.injector.getInstance(classOf[KitchenService])

    @tailrec
    def doit(search: String, rv: List[Kitchen] = Nil /*, cuisines: List[Long] , location: List[Double]*/): List[Kitchen] = {
      // collect up to limit or more
      if (rv.length >= limit)
        return rv

      // search
      val fetch = kserv.search(
        cuisines, // cuisines
        Option(name).getOrElse(""), // name
        location, // location
        start, limit // bounds
        )._1

      // set term condition
      fetch.size match {
        case 0 =>
          // no matches
          start = -1
          rv ::: fetch
        case n =>
          // move offset
          start += limit

          if (n < limit)
            // not full, recurse
            doit(search, rv ::: fetch)
          else
            // full result, return
            rv ::: fetch
      }
    }

    //rv :::= InitListener.injector.getInstance(classOf[KitchenService]).search(
    //  cuisines, Option(name).getOrElse(""), if (coords.isEmpty) Nil else distance :: coords, start, 25)._1
    val kitchens = doit(Option(name).getOrElse(""))
        //,coords match { case Nil => Nil; case _ => distance.toDouble :: coords })

    // format tags
    val cs = getContext.getCuisines
    for (
      kitchen <- kitchens;
      kc <- kitchen.cuisines;
      cuisine <- cs if (cuisine.getId == kc.getId)
    ) kitchen.tags = kitchen.tags concat ", " concat cuisine.name

    // drop leading separator
    for (kitchen <- kitchens)
      kitchen.tags = kitchen.tags.drop(2)

    // assign results
    setMessage("no results")
    setResult(kitchens)
    new ForwardResolution("/WEB-INF/jsp/search.jsp")
  }
}