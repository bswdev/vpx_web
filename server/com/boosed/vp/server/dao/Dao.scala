package com.boosed.vp.server.dao
import com.boosed.orm.gae.dao.GenericDao
import com.boosed.orm.gae.dao.Locatable
import com.boosed.vp.shared.db._
import com.boosed.orm.gae.dao.impl.GenericDaoImpl

// definition of trivial daos

// interfaces
trait AccountDao extends GenericDao[Account] { self: impl.AccountDaoImpl => }
trait CategoryDao extends GenericDao[Category] { self: impl.CategoryDaoImpl => }
trait CommentDao extends GenericDao[Comment] {self: impl.CommentDaoImpl => }
trait CuisineDao extends GenericDao[Cuisine] { self: impl.CuisineDaoImpl => }
trait DishDao extends GenericDao[Dish] { self: impl.DishDaoImpl => }
trait KitchenDao extends GenericDao[Kitchen] with Locatable { self: impl.KitchenDaoImpl => }
trait TagDao extends GenericDao[Tag] { self: impl.TagDaoImpl => }

// implementations
package impl {
  class AccountDaoImpl extends GenericDaoImpl(classOf[Account]) with AccountDao
  class CategoryDaoImpl extends GenericDaoImpl(classOf[Category]) with CategoryDao
  class CommentDaoImpl extends GenericDaoImpl(classOf[Comment]) with CommentDao
  class CuisineDaoImpl extends GenericDaoImpl(classOf[Cuisine]) with CuisineDao
  class DishDaoImpl extends GenericDaoImpl(classOf[Dish]) with DishDao
  class KitchenDaoImpl extends GenericDaoImpl(classOf[Kitchen]) with KitchenDao
  class TagDaoImpl extends GenericDaoImpl(classOf[Tag]) with TagDao
}