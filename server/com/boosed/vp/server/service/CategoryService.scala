package com.boosed.vp.server.service
import com.boosed.orm.gae.service.GenericService
import com.googlecode.objectify.Key
import com.boosed.vp.shared.db._
import scala.collection.mutable.Map
import com.google.appengine.api.users.User
import com.boosed.vp.server.traits._

trait CategoryService
  extends GenericService[Category]
  with Countable[Category]
  with Escapable
  with Keyable
  with Verifiable[Category] { self: impl.CategoryServiceImpl =>

  // get by a category filtered by account
  def getDishes(category: Long)(implicit user: Key[Account]): List[Dish]

  // delete a category
  def drop(category: Long)(implicit user: Key[Account])

  // save from map, ensures that user is owner
  def save(c: => Option[Category], vs: Map[String, String])(implicit user: Key[Account]): Category

  override def decrement(c: Category) = c.total -= 1
  override def increment(c: Category) = c.total += 1

  // implemented as vals
  //  override val decrement = (c: (Key[Category], Category)) => { c._2.total -= 1; c._2 }
  //  override val increment = (c: (Key[Category], Category)) => { c._2.total += 1; c._2 }
}