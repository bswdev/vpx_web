package com.boosed.vp.server.service
import com.boosed.orm.gae.service.GenericService
import com.googlecode.objectify.Key
import com.boosed.vp.shared.db.Account
import com.boosed.vp.shared.db.Kitchen
import com.boosed.vp.shared.db.Tag
import com.boosed.vp.shared.db.enums.TagType

trait TagService extends GenericService[Tag] { self: impl.TagServiceImpl =>

  def verified(t: TagType, cursor: String)(implicit user: Key[Account]): List[Kitchen] Pair String
}