package com.boosed.vp.server.service
import com.boosed.orm.gae.service.GenericService
import com.boosed.vp.server.traits.Countable
import com.boosed.vp.shared.db.Cuisine

trait CuisineService
  extends GenericService[Cuisine]
  with Countable[Cuisine] { self: impl.CuisineServiceImpl =>

  // only example of context bounds that is less verbose than implicit parameter
  // upper-bounded since we desire implicit evidence of a context bound w/ specific type (i.e., not generalized)
  def save[A <: Boolean: Option](name: String)

  override def decrement(c: Cuisine) = c.total -= 1
  override def increment(c: Cuisine) = c.total += 1
}