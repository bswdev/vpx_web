package com.boosed.vp.server.service.impl
import com.boosed.orm.gae.dao.crit._
import com.boosed.orm.gae.service.impl.GenericServiceImpl
import com.boosed.vp.server.dao.TagDao
import com.boosed.vp.server.service.TagService
import com.boosed.vp.shared.db._
import com.boosed.vp.shared.db.enums.TagType
import com.googlecode.objectify.Key
import com.google.inject.Inject
import scala.collection.JavaConversions._

class TagServiceImpl @Inject() (dao: TagDao)
  extends GenericServiceImpl(dao)
  with TagService {

  override def verified(t: TagType, cursor: String)(implicit user: Key[Account]): List[Kitchen] Pair String = {
    // sort by created desc (latest will appear towards top of list)
    val (results, newCursor) = getParents[Kitchen](
      SortCriterion("-created"), // created descending
      CursorCriterion(cursor), // cursor (if available)
      EQCriterion("account", user), // user account
      EQCriterion("type", t)) // tag type

    // return list sorted by name if type is MANAGED
    val list = results.values.toList
    (if (t == TagType.MANAGED) list.sortBy(_.name) else list) -> newCursor
  }
}