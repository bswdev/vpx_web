package com.boosed.vp.server.service.impl

import com.boosed.vp.shared.db._
import com.google.inject.Inject
import com.google.inject.name.Named
import com.boosed.orm.gae.dao.crit._
import com.boosed.vp.server.dao.DishDao
import com.boosed.vp.server.service._
import com.boosed.vp.shared.db._
import com.boosed.vp.shared.db.embed.Image
import com.boosed.vp.shared.exception._
import com.googlecode.objectify.Key
import scala.collection.JavaConversions._
import scala.collection.mutable.Map
import com.boosed.orm.gae.service.impl.GenericServiceImpl

// max number of categories assignable to dish
class DishServiceImpl @Inject() (
  dao: DishDao,
  @Named("category_limit") limit: Int,
  @Named("price_format") format: String)
  extends GenericServiceImpl(dao)
  with DishService {

  // override string version to parse long id; throw NFE for number format errors
  override def get(id: String): Option[Dish] = try get(id.toLong) catch { case _ => throw new InvalidIdException }

  override def drop(dish: Long)(implicit user: Key[Account]) {
    // access verified
    val d = verified(dish)

    // delete existing image
    dropImage(d.image)

    // drop comments
    dropComments(d: Key[Dish])

    // delete dish
    drop(d)

    // update category totals
    categoryService.updateCounts(d.categories.toList, Nil)
  }

  override def save(d: => Option[Dish], vs: Map[String, String])(implicit user: Key[Account]): Dish = d match {
    // doesn't exist
    case None => throw new NotFoundException

    // found dish
    case Some(d) =>
      // explicitly remove "kitchen" value (since "left" is evaluated by-name when using "toRight")
      val kitchen = vs.remove("kitchen")

      // check for assigned kitchen
      Option(d.kitchen).toLeft(kitchen).fold(
        // dish exists, verify ownership
        _ => if (!owns(d)) throw new UnauthorizedException,

        // new dish (no kitchen/null), check kitchen id (i.e., "right" above)
        {
          // evaluate kitchen id
          case Some(id) => kitchenService.get(id).toLeft(()).fold(
            // check if user owns kitchen
            k => if (user == k.account) d.kitchen = k else throw new UnauthorizedException,
            _ => throw new NotFoundException)

          // kitchen id not provided for new dish
          case None => throw new EntityUpdateException
        })

      // capture old categories and clear
      val decs = d.categories.toList
      d.categories.clear

      // clear rates
      d.rates.clear

      // regex w/ no matching groups
      val PriceRE = """price.*""".r
        
      // parameters
      for ((k, v) <- vs) k match {
        case "name" => d.name = v.trim
        case "desc" => d.desc = v.trim
        //case "price" => d.price = v.trim match {
        //  case "" => 0
        //  case p => (format format p.toDouble).toDouble
        //}
        case PriceRE() => try {
          val rate = v.trim.split("=", 2)
          d.rates += rate(0) -> rate(1)
        } catch {
          // price has incorrect format
          case _ => throw new EntityUpdateException
        }
        case x => try
          //v.trim match {
          // category
          //case "on" => 
          // match on id AND kitchen (should only return 1 category)
          categoryService.get(
            EQCriterion( /*"__key__", new Key(classOf[Category], id)*/ "id", x.toLong),
            EQCriterion("kitchen", d.kitchen))._1.foreach(d.categories.add(_))
        catch {
          // NumberFormatException for category id
          case _ => throw new NotFoundException
        }

        //          // x = price1, price2, etc.
        //          // p (i.e., rate) = price=unit
        //          case p =>
        //            val rate = p.splitAt(p.indexOf('='))
        //            d.rates(x) = p//d.rates += x -> p //(format format p.toDouble).toDouble
        //      }
      }

      // validation
      if (d.categories.isEmpty ||
        d.categories.size > limit ||
        d.name.isEmpty) throw new EntityUpdateException

      // save
      //val rv = dao.persist(d).head._2

      // update totals
      categoryService.updateCounts(decs, d.categories.toList)

      // update dish
      dao.persist(d).head._2 //rv
  }

  override def addImage(dish: Dish, image: Image)(implicit user: Key[Account]) =
    if (owns(dish)) {
      // delete existing image
      dropImage(dish.image)

      // replace image & save
      dish.image = image
      save(dish)
    } else throw new UnauthorizedException

  override def owns(d: Dish)(implicit user: Key[Account]): Boolean =
    kitchenService.get(d.kitchen).getOrElse(throw new NotFoundException).account == user

  // injections
  @Inject var categoryService: CategoryService = _
  @Inject var kitchenService: KitchenService = _
}