package com.boosed.vp.server.service.impl

import com.google.inject.Inject
import com.google.inject.name.Named
import com.beoui.geocell.GeocellManager
import com.beoui.geocell.model.Point
import com.boosed.orm.gae.dao.crit._
import com.boosed.vp.server.dao.KitchenDao
import com.boosed.vp.server.service._
import com.boosed.vp.shared.db._
import com.boosed.vp.shared.db.embed.Image
import com.boosed.vp.shared.db.enums._
import com.boosed.vp.shared.exception._
import com.googlecode.objectify.Key
import scala.collection.JavaConversions._
import scala.collection.mutable.Map
import com.boosed.orm.gae.service.impl.GenericServiceImpl

// max number of associated cuisines
class KitchenServiceImpl @Inject() (
  dao: KitchenDao,
  @Named("cuisine_limit") limit: Int,
  @Named("distance_format") format: String)
  extends GenericServiceImpl(dao)
  with KitchenService {

  override def get(id: String): Option[Kitchen] = try get(id.toLong) catch { case _ => throw new InvalidIdException }

  override def getCategories(kitchen: Long)(implicit user: Key[Account]): List[Category] = get(kitchen) match {
    case Some(k) =>
      val rv = categoryService.get(SortCriterion("name"), EQCriterion("kitchen", k: Key[Kitchen]))._1
      // return filtered by ownership
      if (owns(k)) rv else rv.filterNot(_.name == "uncategorized")
    // kitchen doesn't exist
    case None => throw new NotFoundException
  }

  override def drop(kitchen: Long)(implicit user: Key[Account]) {
    //    for (c <- categoryService.verified(kitchen)) {
    //      // delete dishes
    //      dishService.drop(
    //          dishService.get(SortCriterion("name"), EQCriterion("categories", new Key(classOf[Category], c.id.longValue)))._1:_*
    //      )
    //      
    //      // delete category
    //      categoryService.drop(c)
    //    }

    // kitchen
    val k = verified(kitchen)
    val key: Key[Kitchen] = k

    // categories
    val cs = categoryService.get(SortCriterion("name"), EQCriterion("kitchen", key))._1

    // dishes
    cs.foreach(c => {
      val ds = dishService.get(SortCriterion("name"), EQCriterion("categories", c: Key[Category]))._1

      // delete dish images
      ds.foreach(d => dropImage(d.image))

      // drop dishes
      dishService.drop(ds: _*)
    })

    // drop categories
    categoryService.drop(cs: _*)

    // drop comments
    dropComments(key)

    // drop tags
    tagService.drop(tagService.get(AncestorCriterion(key))._1: _*)

    // delete kitchen image
    dropImage(k.image)

    // delete kitchen
    drop(k)

    // update cuisine totals
    cuisineService.updateCounts(k.cuisines.toList, Nil)
  }

  override def save(k: => Option[Kitchen], vs: Map[String, String])(implicit user: Key[Account]): Kitchen = k match {
    // doesn't exist
    case None => throw new NotFoundException

    // kitchen found
    case Some(k) =>
      // has owner?
      Option(k.account).toLeft(user).fold(
        // kitchen exists, verify ownership
        _ => if (!owns(k)) throw new UnauthorizedException,

        // new, assign account to kitchen
        k.account = _)

      // capture old cuisines and clear
      val decs = k.cuisines.toList
      k.cuisines.clear

      // regex for url
      val REurl = """http[^\s]+||[\s]+""".r

      def makeUrl(url: String): String = url match {
        case REurl() => url // leave blank/unchanged
        case _ => "http://" concat url // add scheme
      }

      // parameters
      for ((ke, v) <- vs) ke match {
        case "name" =>
          k.name = v.trim // value
          k.search = k.name.toUpperCase // index to search
        case "desc" => k.desc = v.trim
        case "phone" => k.phone = escapeHtml(v.trim)
        case "street1" => k.address.street1 = escapeHtml(v.trim)
        case "street2" => k.address.street2 = escapeHtml(v.trim)
        case "city" => k.address.city = escapeHtml(v.trim)
        case "statoid" => k.address.statoid = escapeHtml(v.trim)
        case "postal" => k.address.postal = escapeHtml(v.trim)
        case "country" => k.address.country = escapeHtml(v.trim)
        case "lat" => k.latitude = v.toDouble
        case "lon" => k.longitude = v.toDouble
        case "homepage" => k.homepage = makeUrl(v.trim)
        case "facebook" => k.facebook = makeUrl(v.trim)
        case "alternate" => k.alternate = makeUrl(v.trim)
        // add cuisine types (from map keys)
        case c => cuisineService.get(c).foreach(k.cuisines.add(_))
      }

      // validation
      if (k.cuisines.isEmpty ||
        k.cuisines.size > limit ||
        k.name.isEmpty ||
        k.latitude == 0 ||
        k.longitude == 0 ||
        k.address.city.isEmpty ||
        k.address.country.isEmpty ||
        k.address.postal.isEmpty) throw new EntityUpdateException

      // geocode
      //      val l = Geocoder.geocode(k.address.toString)
      //      k.latitude = l._1
      //      k.longitude = l._2
      k.location = GeocellManager.generateGeoCell(new Point(k.latitude, k.longitude))

      // update cuisines
      cuisineService.updateCounts(decs, k.cuisines.toList)

      // check if the tag should be created
      Option(k.id) match {
        case Some(_) => dao.persist(k).head._2
        case None =>
          // new kitchen, create tag
          val rv = dao.persist(k).head._2
          tagService.save(new Tag(rv: Key[Kitchen], user, TagType.MANAGED))
          rv
      }
  }

  override def search(
    cuisines: List[Long],
    search: String,
    location: List[Double],
    offset: Int,
    limit: Int): List[Kitchen] Pair String = {
    
    val noloc = location.isEmpty

    try {
      val (results, cursor) = dao.find(OffsetCriterion(offset) // offset
        :: LimitCriterion(limit) // limit
        :: StartsWithCriterion("search", search.toUpperCase) // search, use "" to reduce number of index writes
        //        :: { // permit blank searches to reduce number of indexes
        //          if (search.isEmpty) EmptyCriterion() else
        //            StartsWithCriterion("search", search.toUpperCase)
        //        }
        :: SortCriterion("priority") // priority, inequality filter (i.e., "search") must be 1st sort
        :: { // cuisines
          if (cuisines.isEmpty) Nil
          else List(InCriterion("cuisines", cuisines.map(new Key(classOf[Cuisine], _))))
        }
        ::: { // location (w/ sort if not provided); sort by time last
          if (noloc) List(SortCriterion("-listed"))
          else List(dao.within(location(0), location(1) -> location(2)), SortCriterion("search"), SortCriterion("-listed"))
        }: _*)(if (noloc) passthru else {
        // filter out items further than supplied radius
        val radius = location(0)
        val origin = location(1) -> location(2)
        k => {
          //k.distance = dao.distance(origin, k.latitude -> k.longitude)
          k.distance = (format format dao.distance(origin, k.latitude -> k.longitude)).toDouble
          k.distance <= radius
        }
      })

      // secondary sort by distance if location provided
      if (noloc) results.sortBy(_.priority) -> cursor
      else results.sortBy(k => (k.priority, k.distance)) -> cursor
    } catch {
      // capture when ID cannot be zero and rethrow as InvalidIdException

      case _: IllegalArgumentException => throw new InvalidIdException
    }
  }

  override def addImage(kitchen: Kitchen, image: Image)(implicit user: Key[Account]) =
    if (owns(kitchen)) {
      // delete existing image
      dropImage(kitchen.image)

      // replace image & save
      kitchen.image = image
      save(kitchen)
    } else throw new UnauthorizedException

  override def owns(k: Kitchen)(implicit user: Key[Account]) = k.account == user

  // injections
  @Inject var categoryService: CategoryService = _
  @Inject var cuisineService: CuisineService = _
  @Inject var dishService: DishService = _
  @Inject var tagService: TagService = _
}