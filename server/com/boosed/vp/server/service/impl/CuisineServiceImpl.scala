package com.boosed.vp.server.service.impl
import com.googlecode.objectify.Key
import com.google.inject.Inject
import com.boosed.vp.server.dao.CuisineDao
import com.boosed.vp.server.service.CuisineService
import com.boosed.vp.shared.db.Cuisine
import com.boosed.vp.shared.exception._
import scala.collection.JavaConversions.asScalaMap
import com.boosed.orm.gae.service.impl.GenericServiceImpl

class CuisineServiceImpl @Inject() (dao: CuisineDao)
  extends GenericServiceImpl(dao)
  with CuisineService {

  override def get(id: String): Option[Cuisine] = try get(id.toLong) catch { case _ => throw new InvalidIdException }

  override def save[T <: Boolean: Option](name: String) = name.trim match {
    // validate name
    case "" => throw new EntityUpdateException
    case x => save(new Cuisine(x))
  }
}