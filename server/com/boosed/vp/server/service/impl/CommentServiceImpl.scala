package com.boosed.vp.server.service.impl
import com.boosed.orm.gae.dao.crit._
import com.boosed.orm.gae.service.impl.GenericServiceImpl
import com.boosed.vp.server.dao.CommentDao
import com.boosed.vp.server.service._
import com.boosed.vp.shared.db._
import com.boosed.vp.shared.db.embed._
import com.boosed.vp.shared.exception._
import com.googlecode.objectify.Key
import com.google.inject.Inject
import scala.collection.JavaConversions._
import scala.collection.mutable.Map

class CommentServiceImpl @Inject() (dao: CommentDao)
  extends GenericServiceImpl(dao)
  with CommentService {

  override def get(id: String): Option[Comment] = try get(id.toLong) catch { case _ => throw new InvalidIdException }

  override def get(subject: Key[_])(implicit user: Key[Account]): Option[Comment] = try
    Option(dao.find(
      AncestorCriterion(subject),
      EQCriterion("account", user))._1.head)
  catch {
    case _ => None
  }

  override def getComments(subject: Key[_], limit: Int, cursor: String): (List[Comment], String) =
    dao.find(
      SortCriterion("-created"),
      AncestorCriterion(subject),
      EQCriterion("isComment", true),
      CursorCriterion(cursor),
      LimitCriterion(limit))

  override def getImages(subject: Key[_], limit: Int, cursor: String): (List[Comment], String) =
    dao.find(
      SortCriterion("-created"),
      AncestorCriterion(subject),
      EQCriterion("isImage", true),
      CursorCriterion(cursor),
      LimitCriterion(limit))

  def drop(comment: Long)(implicit user: Key[Account]) {
    // access verified
    val c = verified(comment)

    // delete existing image
    dropImage(c.image)

    // decrement recommends
    if (c.recommend)
      recommend(c.subject, false)

    // delete comment
    drop(c)
  }

  //  override def save(c: => Option[Comment], vs: Map[String, String])(implicit user: Key[Account]): Comment = c match {
  //    case Some(c) =>
  //      Option(c.account).toLeft(()).fold(
  //        // existing, check if accounts match
  //        _ => if (!owns(c)) throw new UnauthorizedException,
  //        // no associated account
  //        _ => throw new NotFoundException) //c.account = user
  //
  //      // current recommendation
  //      val recommend = c.recommend
  //
  //      // parameters
  //      for ((k, v) <- vs) k match {
  //        case "comment" => c.comment = v.trim
  //        case "recommend" => c.recommend = try v.trim.toBoolean catch { case _ => throw new EntityUpdateException }
  //        case _ => //warning("the parameter " + x + " has value " + v.trim)// ignore extraneous parameters
  //      }
  //
  //      for ((k, v) <- vs)
  //        warning("the value of " + k + " is " + v)
  //        
  //      // validation
  //      //if (c.name.isEmpty) throw new EntityUpdateException
  //
  //      // save & return
  //      //val rv = dao.persist(c).head._2
  //
  //      // update stats
  //      if (recommend ^ c.recommend)
  //        // change in recommendation
  //        updateRecommend(c, if (c.recommend) 1 else -1)
  //
  //      dao.persist(c).head._2 //rv
  //    // doesn't exist
  //    case None => throw new NotFoundException
  //  }

  override def save(subject: Key[_], vs: Map[String, String])(implicit user: Key[Account]): Comment = {
    // retrieve comment
    val c = get(AncestorCriterion(subject), EQCriterion("account", user))._1 match {
      case c :: cs => if (owns(c)) c else throw new UnauthorizedException
      // new comment
      case _ => new Comment(subject, user)
    }

    // capture current recommendation & initialize to false
    val oldRec = c.recommend
    c.recommend = false

    // anonymize
    var anonymize = false

    // parameters
    for ((k, v) <- vs) k match {
      case "comment" => c.comment = v.trim
      case "recommend" => c.recommend = true // if param is present, mark true
      case "anonymize" => anonymize = true
      //try v.trim.toBoolean catch { case _ => throw new EntityUpdateException }
      case _ => // ignore extraneous parameters
    }

    // comment handle
    if (anonymize) c.handle = "anonymous"
    else c.handle = accountService.get(user).getOrElse(throw new NotFoundException).name

    // validation
    //    if (c.comment.isEmpty &&
    //      !c.recommend) throw new EntityUpdateException

    // save & return
    //val rv = dao.persist(c).head._2

    // change in recommendation
    if (oldRec ^ c.recommend)
      recommend(c.subject, c.recommend)

    dao.persist(c).head._2 //rv
  }

  override def addImage(comment: Comment, image: Image)(implicit user: Key[Account]) =
    if (owns(comment)) {
      // delete existing image
      dropImage(comment.image)

      // replace image & save
      comment.image = image
      save(comment)
    } else throw new UnauthorizedException

  override def owns(c: Comment)(implicit user: Key[Account]): Boolean = c.account == user

  // increments first, decrements second (i.e., create an empty list in second OR first position)
  private def recommend(subject: Key[_], increment: Boolean) = subject.getKindClassName match {
    case "com.boosed.vp.shared.db.Dish" =>
      val (decs, incs) = List(subject.asInstanceOf[Key[Dish]]).splitAt(if (increment) 0 else 1)
      dishService.updateCounts(decs, incs)
    case "com.boosed.vp.shared.db.Kitchen" =>
      val (decs, incs) = List(subject.asInstanceOf[Key[Kitchen]]).splitAt(if (increment) 0 else 1)
      kitchenService.updateCounts(decs, incs)
    case _ => throw new EntityUpdateException
  }

  // injects
  @Inject var accountService: AccountService = _
  @Inject var dishService: DishService = _
  @Inject var kitchenService: KitchenService = _
}