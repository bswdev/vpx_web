package com.boosed.vp.server.service.impl
import com.googlecode.objectify.Key
import com.google.inject.Inject
import com.boosed.vp.server.dao.AccountDao
import com.boosed.vp.server.service.AccountService
import com.boosed.vp.shared.db.Account
import com.boosed.vp.shared.exception._
import scala.collection.JavaConversions.asScalaMap
import scala.collection.mutable.Map
import com.boosed.orm.gae.service.impl.GenericServiceImpl
import scala.collection.JavaConversions._

class AccountServiceImpl @Inject() (dao: AccountDao)
  extends GenericServiceImpl(dao)
  with AccountService {
  
  override def save(vs: Map[String, String])(implicit user: Key[Account]): Account = get(user) match {
    case None => throw new NotFoundException
    case Some(a) =>
      // parameters
      for ((k, v) <- vs) k match {
        case "name" => a.name = v.trim
        case "email" => a.email = v.trim
      }

      // validation
      if (a.name.isEmpty ||
        a.email.isEmpty) throw new EntityUpdateException

      // save & return
      dao.persist(a).head._2
  }
}