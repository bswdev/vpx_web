package com.boosed.vp.server.service.impl

import com.boosed.vp.shared.db._
import com.googlecode.objectify.Key
import com.google.inject.Inject
import com.boosed.orm.gae.dao.crit._
import com.boosed.vp.server.dao.CategoryDao
import com.boosed.vp.server.service._
import com.boosed.vp.shared.exception._
import scala.collection.JavaConversions._
import scala.collection.mutable.Map
import com.boosed.orm.gae.service.impl.GenericServiceImpl

class CategoryServiceImpl @Inject() (dao: CategoryDao)
  extends GenericServiceImpl(dao)
  with CategoryService {

  // override string version to parse long id; throw NFE for number format errors
  override def get(id: String): Option[Category] = try get(id.toLong) catch { case _ => throw new InvalidIdException }

  override def getDishes(category: Long)(implicit user: Key[Account]): List[Dish] = get(category) match {
    case Some(c) =>
      val rv = dishService.get(SortCriterion("name"), EQCriterion("categories", c: Key[Category]))._1
      // return filtered by ownership
      if (owns(c)) rv else rv.filter(_.visible)
    case None => throw new NotFoundException
  }

  override def drop(category: Long)(implicit user: Key[Account]) {
    // access verified
    val c = verified(category)

    // process dishes
    val ds = dishService.get(EQCriterion("categories", c: Key[Category]))._1
    if (!ds.isEmpty) {
      // user attempting to delete non-empty "uncategorized" category
      if (c.name == "uncategorized")
        throw new NonEmptyException

      // remove category from dishes
      val key: Key[Category] = c
      ds.foreach(_.categories.remove(key))

      // check for orphans
      if ((false /: ds)(_ || _.categories.isEmpty)) {
        // get "uncategorized" category
        val (uncatKey, uncat) = get(EQCriterion("name", "uncategorized"))._1 match {
          // use existing
          case c :: cs => (c: Key[Category]) -> c
          // create new
          case Nil => save(new Category("uncategorized", c.kitchen)).head
        }

        // update orphans
        ds.filter(_.categories.isEmpty).foreach { d =>
          // add "uncategorized" key
          d.categories.add(uncatKey)

          // set invisible
          d.visible = false

          // increment
          uncat.total += 1
        }

        // save "uncategorized"
        save(uncat)
      }

      // save changes to dishes
      dishService.save(ds: _*)
    }

    // delete category
    drop(c)
  }

  override def save(c: => Option[Category], vs: Map[String, String])(implicit user: Key[Account]): Category = c match {
    // doesn't exist
    case None => throw new NotFoundException

    // found category
    case Some(c) =>
      // owner?
      val kitchen = vs.remove("kitchen") // explicitly remove "kitchen" value
      Option(c.kitchen).toLeft(kitchen).fold(
        // category exists, verify ownership
        _ => if (!owns(c)) throw new UnauthorizedException,

        // new category (no kitchen/null), check kitchen id
        {
          // kitchen id provided, perform lookup
          case Some(id) => kitchenService.get(id).toLeft(()).fold(
            // check if user owns kitchen
            k => if (user == k.account) c.kitchen = k else throw new UnauthorizedException,
            _ => throw new NotFoundException)
          // kitchen id not provided for new category
          case None => throw new EntityUpdateException
        })

      // parameters
      for ((k, v) <- vs) k match {
        case "name" => c.name = escapeHtml(v.trim)
        case "desc" => c.desc = escapeHtml(v.trim)
      }

      // validation
      if (c.name.isEmpty) throw new EntityUpdateException

      // save & return
      dao.persist(c).head._2
  }

  override def owns(c: Category)(implicit user: Key[Account]): Boolean =
    kitchenService.get(c.kitchen).getOrElse(throw new NotFoundException).account == user

  // injections
  @Inject var dishService: DishService = _
  @Inject var kitchenService: KitchenService = _
}