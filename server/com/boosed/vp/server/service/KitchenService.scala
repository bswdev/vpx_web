package com.boosed.vp.server.service
import com.boosed.orm.gae.service.GenericService
import com.boosed.vp.server.traits.Countable
import com.googlecode.objectify.Key
import com.boosed.vp.server.traits.Commentable
import com.boosed.vp.server.traits.Escapable
import com.boosed.vp.server.traits.Verifiable
import com.boosed.vp.shared.db.Account
import com.boosed.vp.shared.db.Category
import com.boosed.vp.shared.db.Kitchen
import scala.collection.mutable.Map

trait KitchenService
  extends GenericService[Kitchen]
  with Escapable
  with Commentable[Kitchen]
  with Countable[Kitchen]
  with Verifiable[Kitchen] { self: impl.KitchenServiceImpl =>

  // get by kitchen filtered by account
  def getCategories(kitchen: Long)(implicit user: Key[Account]): List[Category]

  // delete a kitchen
  def drop(kitchen: Long)(implicit user: Key[Account])

  // kitchen is pass-by-name (i.e., function param) so that account is evaluated before kitchen 
  def save(k: => Option[Kitchen], vs: Map[String, String])(implicit user: Key[Account]): Kitchen

  // includes default arguments
  def search(cuisines: List[Long] = Nil,
    search: String = "",
    location: List[Double] = Nil,
    offset: Int = 0,
    limit: Int = 25): List[Kitchen] Pair String

  override def decrement(k: Kitchen) = k.recommends -= 1
  override def increment(k: Kitchen) = k.recommends += 1
}