package com.boosed.vp.server.service
import com.boosed.orm.gae.service.GenericService
import com.googlecode.objectify.Key
import com.boosed.vp.shared.db._
import scala.collection.mutable.Map
import com.boosed.vp.shared.db.embed.Image
import com.boosed.vp.server.traits._

trait DishService
  extends GenericService[Dish]
  with Commentable[Dish]
  with Countable[Dish]
  with Toggleable[Dish]
  with Verifiable[Dish] { self: impl.DishServiceImpl =>

  // delete a dish
  def drop(dish: Long)(implicit user: Key[Account])

  // save a dish, ensures user is owner
  def save(d: => Option[Dish], vs: Map[String, String])(implicit user: Key[Account]): Dish

  // toggle visibility of dish
  //def toggle(dish: Long)(implicit user: Key[Account]): Boolean

  override def decrement(d: Dish) = d.recommends -= 1
  override def increment(d: Dish) = d.recommends += 1
  override def invert(id: String)(implicit user: Key[Account], admin: Option[Boolean]): Dish = {
    // ensure user is owner
    val d = verified(id)
    
    // toggle visibility and return
    d.visible = !d.visible
    d
  }
}