package com.boosed.vp.server.service
import com.boosed.orm.gae.service.GenericService
import com.boosed.vp.shared.db.Account
import com.googlecode.objectify.Key
import scala.collection.mutable.Map
import com.boosed.vp.server.traits._
import com.boosed.vp.shared.exception.NotFoundException

trait AccountService
  extends GenericService[Account]
  with Toggleable[Account] { self: impl.AccountServiceImpl =>

  // save from map
  def save(vs: Map[String, String])(implicit user: Key[Account]): Account

  override def invert(id: String)(implicit user: Key[Account], admin: Option[Boolean]): Account =
    get(id).toLeft(()).fold(
      // exists, flip enabled and return
      a => { a.enabled = !a.enabled; a },
      _ => throw new NotFoundException)
}