package com.boosed.vp.server.service
import com.boosed.orm.gae.service.GenericService
import com.googlecode.objectify.Key
import com.boosed.vp.shared.db._
import com.boosed.vp.shared.db.embed._
import scala.collection.mutable.Map
import com.boosed.vp.server.traits._

trait CommentService
  extends GenericService[Comment]
  with Imageable[Comment]
  with Verifiable[Comment] { self: impl.CommentServiceImpl =>

  // retrieve a user comment on specific subject
  def get(subject: Key[_])(implicit user: Key[Account]): Option[Comment]

  // retrieve comments for a subject
  def getComments(subject: Key[_], limit: Int = 25, cursor: String = null): (List[Comment], String)

  // retrieve images for a subject
  def getImages(subject: Key[_], limit: Int = 25, cursor: String = null): (List[Comment], String)

  // delete a comment
  def drop(comment: Long)(implicit user: Key[Account])

  // save comment for given subject
  def save(subject: Key[_], vs: Map[String, String])(implicit user: Key[Account]): Comment

  // save existing comment
  //def save(c: => Option[Comment], vs: Map[String, String])(implicit user: Key[Account]): Comment
}