package com.boosed.vp.server.conf

import com.boosed.vp.server.servlet._
//import com.boosed.vp.server.rs.RestServlet
import scala.collection.JavaConversions._

class ServletModule extends com.google.inject.servlet.ServletModule {

  override protected def configureServlets {

    // comment save/update
    serve("/comment") `with` classOf[CommentServlet]

    // dish save/update
    serve("/dish") `with` classOf[DishServlet]

    // kitchen image
    serve("/image") `with` classOf[ImageServlet]

    // rpc
    serve("/mgr/rpc", "/mnu/rpc", "/mob/rpc") `with` classOf[RpcServlet]

    // rest service
    //serve("/rs/*") `with` (classOf[RestServlet], Map(
    //  "org.restlet.application" -> "com.boosed.vp.server.rs.RestApplication"))

    // we do this only for injection purposes (to static fields)
    //serve("/injected") `with` classOf[ServiceServlet]
  }
}