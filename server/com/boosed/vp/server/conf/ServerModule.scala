package com.boosed.vp.server.conf
import com.boosed.vp.server.dao._
import com.boosed.vp.server.dao.impl._
import com.boosed.vp.server.service._
import com.boosed.vp.server.service.impl._
import com.google.appengine.api.blobstore.BlobstoreService
import com.google.appengine.api.blobstore.BlobstoreServiceFactory
import com.google.appengine.api.images.ImagesService
import com.google.appengine.api.users.UserService
import com.google.appengine.api.users.UserServiceFactory
import com.google.inject.AbstractModule
import com.google.inject.Provides
import com.google.inject.name.Names
import com.google.appengine.api.images.ImagesServiceFactory
import com.google.appengine.api.files.FileService
import com.google.appengine.api.files.FileServiceFactory

object ServerModule {
  
  // CONFIGURATION PARAMETERS

  // limit for the number of values in a list property (e.g., kitchen.cuisines or dish.categories)
  private val listPropertyLimit = 5

  // 4 x 3 (160) image
  private val imageWidth = 640
  private val imageHeight = 480
  
  // formats
  private val priceFormat = "%.2f"
  private val distanceFormat = "%.2f"

  // GAE services via @Provides
  private var blobstoreService: BlobstoreService = _
  private var fileService: FileService = _
  private var imagesService: ImagesService = _
  private var userService: UserService = _
}

class ServerModule extends AbstractModule {

  override protected def configure {

    // limits
    bind(classOf[Int]) annotatedWith Names.named("category_limit") toInstance (ServerModule.listPropertyLimit)
    bind(classOf[Int]) annotatedWith Names.named("cuisine_limit") toInstance (ServerModule.listPropertyLimit)

    // image dimensions
    bind(classOf[Int]) annotatedWith Names.named("image_height") toInstance (ServerModule.imageHeight)
    bind(classOf[Int]) annotatedWith Names.named("image_width") toInstance (ServerModule.imageWidth)

    // formats
    bind(classOf[String]) annotatedWith Names.named("distance_format") toInstance (ServerModule.distanceFormat)
    bind(classOf[String]) annotatedWith Names.named("price_format") toInstance (ServerModule.priceFormat)
    
    // daos
    bind(classOf[AccountDao]).to(classOf[AccountDaoImpl]).asEagerSingleton
    bind(classOf[CategoryDao]).to(classOf[CategoryDaoImpl]).asEagerSingleton
    bind(classOf[CommentDao]).to(classOf[CommentDaoImpl]).asEagerSingleton
    bind(classOf[CuisineDao]).to(classOf[CuisineDaoImpl]).asEagerSingleton
    bind(classOf[DishDao]).to(classOf[DishDaoImpl]).asEagerSingleton
    bind(classOf[KitchenDao]).to(classOf[KitchenDaoImpl]).asEagerSingleton
    bind(classOf[TagDao]).to(classOf[TagDaoImpl]).asEagerSingleton

    // services
    bind(classOf[AccountService]).to(classOf[AccountServiceImpl]).asEagerSingleton
    bind(classOf[CategoryService]).to(classOf[CategoryServiceImpl]).asEagerSingleton
    bind(classOf[CommentService]).to(classOf[CommentServiceImpl]).asEagerSingleton
    bind(classOf[CuisineService]).to(classOf[CuisineServiceImpl]).asEagerSingleton
    bind(classOf[DishService]).to(classOf[DishServiceImpl]).asEagerSingleton
    bind(classOf[KitchenService]).to(classOf[KitchenServiceImpl]).asEagerSingleton
    bind(classOf[TagService]).to(classOf[TagServiceImpl]).asEagerSingleton
  }

  @Provides
  def provideBlobstoreService: BlobstoreService = Option(ServerModule.blobstoreService) match {
    case Some(x) => x
    case None =>
      ServerModule.blobstoreService = BlobstoreServiceFactory.getBlobstoreService
      ServerModule.blobstoreService
  }

  @Provides
  def provideFileService: FileService = Option(ServerModule.fileService) match {
    case Some(x) => x
    case None =>
      ServerModule.fileService = FileServiceFactory.getFileService
      ServerModule.fileService
  }

  @Provides
  def provideImagesService: ImagesService = Option(ServerModule.imagesService) match {
    case Some(x) => x
    case None =>
      ServerModule.imagesService = ImagesServiceFactory.getImagesService
      ServerModule.imagesService
  }

  @Provides
  def provideUserService: UserService = Option(ServerModule.userService) match {
    case Some(x) => x
    case None =>
      ServerModule.userService = UserServiceFactory.getUserService
      ServerModule.userService
  }
}