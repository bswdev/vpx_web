package com.boosed.vp.server.rs
import javax.inject.Singleton
import javax.ws.rs.core.Application
import java.util.HashSet
import org.restlet.Context
import org.restlet.engine.Engine
import org.restlet.ext.jackson.JacksonConverter
import org.restlet.ext.jaxrs.JaxRsApplication
import org.restlet.ext.servlet.ServerServlet
import scala.collection.JavaConversions._
import org.codehaus.jackson.map.ObjectMapper
import org.codehaus.jackson.map.module.SimpleModule
import com.googlecode.objectify.Key
import org.codehaus.jackson.Version
import com.boosed.vp.server.rs.serial.KeySerializer
import com.boosed.vp.shared.exception.RemoteServiceFailureException
import javax.ws.rs.core.Response
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider

// application configured to servlet in web.xml
class RestApplication(ctx: Context) extends JaxRsApplication(ctx) {

  // add the application
  add(new Application {
    override def getClasses: java.util.Set[Class[_]] = scala.collection.mutable.Set[Class[_]](
      // configure bindings
      classOf[KitchenServerResource],
      classOf[RemoteExceptionMapper])
  })

  //setGuard(...); // if needed
  //setRoleChecker(...); // if needed

  // use a custom converter for objectify keys
  val cs = Engine.getInstance.getRegisteredConverters
  cs find (_.getClass == classOf[JacksonConverter]) match {
    case None => // do nothing
    case Some(x) =>
      // remove current converter
      cs remove x

      // add a custom converter
      cs add new JacksonConverter {
        override def createObjectMapper: ObjectMapper = {
          // register the custom serializers
          val om = super.createObjectMapper
          om registerModule new ObjectifyJacksonModule
          om
        }
      }
  }

  //  override def createInboundRoot: Restlet = {
  //    val router = new Router(getContext)
  //
  //    router.attachDefault(new Directory(getContext, "war:///"))
  //    router.attach("/kitchen", classOf[KitchenServerResource])
  //  }  
}

// exception mapper to response
@Provider class RemoteExceptionMapper
  extends ExceptionMapper[RemoteServiceFailureException] {
  override def toResponse(exception): Response =
    Response.status(Response.Status.BAD_REQUEST).build
}

// singleton servlet that can be bound by guice
@Singleton class RestServlet extends ServerServlet