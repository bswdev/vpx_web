package com.boosed.vp.server.rs
import com.boosed.vp.server.listener.InitListener
import com.boosed.vp.server.service._
import com.boosed.vp.server.traits.Authorizable
import com.boosed.vp.shared.db._
import com.boosed.vp.shared.rs.KitchenResource
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.PathParam
import javax.ws.rs.Produces
import org.restlet.resource.ServerResource
import org.restlet.resource.Get
import com.google.appengine.api.users.UserServiceFactory
import java.util.ArrayList
import scala.collection.JavaConversions._
import com.boosed.vp.shared.exception.NotFoundException

@Path("/kitchen")
class KitchenServerResource
  extends ServerResource
  with KitchenResource
  with Authorizable {

  // assignments for Authorizable
  accountService = InitListener.injector.getInstance(classOf[AccountService])
  userService = UserServiceFactory.getUserService

  //    @GET
  //    def Map<String, Cluster> getClusters() {
  //        return clusters;
  //    }

  //    @POST
  //    def Response addCluster(Cluster newCluster) {
  //        Response response;
  //        if (clusters.containsKey(newCluster.getName())) {
  //            response = Response.status(Response.Status.BAD_REQUEST)
  //                    .entity("That cluster already exists")
  //                    .build();
  //        } else {
  //            clusters.put(newCluster.getName(), newCluster);
  //            response = Response.ok().build();
  //        }
  //        return response;
  //    }

  @GET
  @Produces(Array("text/html"))
  def default = {
    <html><body>
            id not provided
          </body></html>
  }.toString

  //@Get
  @GET
  @Produces(Array("application/json"))
  @Path("{id}")
  def get(@PathParam("id") id: java.lang.Long): Kitchen =
    kitchenService.get(id.longValue).getOrElse(throw new NotFoundException)

  @GET
  @Produces(Array("application/json"))
  @Path("categories/{id}")
  def categories(@PathParam("id") id: java.lang.Long): java.util.List[Category] = {
    try {
      val rv = kitchenService.getCategories(id.longValue)(accounted)
      //println(rv.head.name)
      new ArrayList(rv)
      rv
    } catch {
      case e => e.printStackTrace; Nil
    }
  }
  // 
  //    @POST
  //    @Path("{name}")
  //    public Response updateCluster(Cluster c) {
  //        c.setModified(new Date());
  //        clusters.put(c.getName(), c);
  //        Response response = Response.ok(c).build();
  //        return response;
  //    }
  // 
  //    @DELETE
  //    @Path("{name}")
  //    public Response deleteCluster(@PathParam("name") String name) {
  //        clusters.remove(name);
  //        return Response.ok().build();
  //    }

  var kitchenService: KitchenService = InitListener.injector.getInstance(classOf[KitchenService])
}