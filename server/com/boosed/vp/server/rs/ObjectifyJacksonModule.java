package com.boosed.vp.server.rs;

import org.codehaus.jackson.Version;
import org.codehaus.jackson.map.module.SimpleModule;

import com.boosed.vp.server.rs.serial.BlobKeyDeserializer;
import com.boosed.vp.server.rs.serial.BlobKeySerializer;
import com.boosed.vp.server.rs.serial.KeyDeserializer;
import com.boosed.vp.server.rs.serial.KeyKeySerializer;
import com.boosed.vp.server.rs.serial.KeySerializer;
import com.boosed.vp.server.rs.serial.RawKeyDeserializer;
import com.boosed.vp.server.rs.serial.RawKeyKeySerializer;
import com.boosed.vp.server.rs.serial.RawKeySerializer;
import com.google.appengine.api.blobstore.BlobKey;
import com.googlecode.objectify.Key;

/**
 * Call jackson's
 * {@code ObjectMapper.registerModule(new ObjectifyJacksonModule())} to enable
 * intelligent serialization and deserialization of various Objectify and GAE
 * classes.
 */
public class ObjectifyJacksonModule extends SimpleModule {

	public ObjectifyJacksonModule() {
		super("Objectify", Version.unknownVersion());

		// objectify key
		addSerializer(Key.class, new KeySerializer());
		addKeySerializer(Key.class, new KeyKeySerializer());
		addDeserializer(Key.class, new KeyDeserializer());

		// native datastore key
		addSerializer(com.google.appengine.api.datastore.Key.class, new RawKeySerializer());
		addKeySerializer(com.google.appengine.api.datastore.Key.class, new RawKeyKeySerializer());
		addDeserializer(com.google.appengine.api.datastore.Key.class, new RawKeyDeserializer());

		// native datastore BlobKey
		addSerializer(BlobKey.class, new BlobKeySerializer());
		addDeserializer(BlobKey.class, new BlobKeyDeserializer());
		
		// exceptions
		//addSerializer(RemoteServiceFailureException.class, new ExceptionSerializer());
	}
}