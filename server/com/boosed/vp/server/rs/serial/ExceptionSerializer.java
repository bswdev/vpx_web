package com.boosed.vp.server.rs.serial;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

import com.boosed.vp.shared.exception.RemoteServiceFailureException;

/**
 * Configuring this serializer will make native datastore Key objects render as
 * their web-safe string.
 */
public class ExceptionSerializer extends JsonSerializer<RemoteServiceFailureException> {

	@Override
	public void serialize(RemoteServiceFailureException value, JsonGenerator jgen, SerializerProvider provider)
			throws IOException, JsonProcessingException {
		jgen.writeString(value.getMessage());
	}
}