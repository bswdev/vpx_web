package com.boosed.vp.server.rs.serial;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

import com.google.appengine.api.blobstore.BlobKey;

/**
 * Configuring this serializer will make BlobKey objects render as their web-safe string.
 */
public class BlobKeySerializer extends JsonSerializer<BlobKey> {

	@Override
	public void serialize(BlobKey value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
		jgen.writeString(value.getKeyString());
	}
}