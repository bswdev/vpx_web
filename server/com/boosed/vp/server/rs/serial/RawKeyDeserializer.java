package com.boosed.vp.server.rs.serial;

import java.io.IOException;

import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.deser.std.StdDeserializer;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

/**
 * Will deserialize a google native datastore Key that was serialized with the RawKeySerializer
 */
public class RawKeyDeserializer extends StdDeserializer<Key> {

	/** */
	public RawKeyDeserializer() {
		super(Key.class);
	}

	/** */
	@Override
	public Key deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		String text = jp.getText();
		return KeyFactory.stringToKey(text);
	}
}