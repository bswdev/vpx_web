package com.boosed.vp.server.rs.serial;

import java.io.IOException;

import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.deser.std.StdDeserializer;

import com.googlecode.objectify.Key;

/**
 * Will deserialize an Objectify Key<?> that was serialized with the KeySerializer
 */
@SuppressWarnings("rawtypes")
public class KeyDeserializer extends StdDeserializer<Key> {

	/** */
	public KeyDeserializer() {
		super(Key.class);
	}

	/** */
	@SuppressWarnings("unchecked")
	@Override
	public Key deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		// TODO this needs to be corrected to accommodate the type of the key
		String text = jp.getText();
		String name = jp.getCurrentName();
		
		try {
			return new Key(Class.forName(name), text);
			//return Key.create(text);
		} catch (Exception e) {
			return null;
		}
	}
}