package com.boosed.vp.server.rs.serial;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

import com.googlecode.objectify.Key;

/**
 * Configuring this serializer will make Key<?> objects render as their web-safe
 * string.
 */
@SuppressWarnings("rawtypes")
public class KeySerializer extends JsonSerializer<Key> {

	@Override
	public void serialize(Key value, JsonGenerator jgen, SerializerProvider provider) throws IOException,
			JsonProcessingException {
		String name = value.getName();
		
		//jgen.writeStringField(value.getKind(), name == null ? Long.toString(value.getId()) : name);
		if (name == null)
			jgen.writeNumber(value.getId());
		else
			jgen.writeString(name);
		
		//jgen.writeFieldName(value.getKind());
		//jgen.writeString(value.getString());
	}
}