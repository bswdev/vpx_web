package com.boosed.vp.server.rs.serial;
import java.io.IOException;

import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.deser.std.StdDeserializer;

import com.google.appengine.api.blobstore.BlobKey;

/**
 * Will deserialize a BlobKey that was serialized with the BlobKeySerializer
 */
public class BlobKeyDeserializer extends StdDeserializer<BlobKey> {

	/** */
	public BlobKeyDeserializer() {
		super(BlobKey.class);
	}

	/** */
	@Override
	public BlobKey deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		String keyStr = jp.getText();
		return new BlobKey(keyStr);
	}
}