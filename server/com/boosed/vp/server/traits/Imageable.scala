package com.boosed.vp.server.traits
import com.google.inject.Inject
import com.boosed.vp.shared.db.Account
import com.boosed.vp.shared.db.embed.Image
import com.google.appengine.api.blobstore.BlobKey
import com.google.appengine.api.blobstore.BlobstoreService
import com.google.appengine.api.files.FileService
import com.google.appengine.api.images.ImagesService
import com.google.appengine.api.images.ImagesServiceFactory
import com.google.appengine.api.images.ImagesService.OutputEncoding
import java.nio.ByteBuffer
import javax.servlet.http.HttpServletRequest
import java.util.logging.Logger
import com.googlecode.objectify.Key

/**
 * Trait for services/servlets which process images.
 */
trait Imageable[T] extends Keyable {

  // must be implemented since we can't use structural types to match against Java entities
  def addImage(t: T, image: Image)(implicit user: Key[Account])

  // retrieve the blob key associated with an image upload
  def getImage(req: HttpServletRequest): BlobKey = blobstoreService.getUploadedBlobs(req).get("image")

  // delete image using image reference
  def dropImage(image: Image): Unit = if (image.key != "")
    // only delete if image is present
    blobstoreService.delete(image.key)

  // delete image using key
  def dropImage(key: BlobKey) = blobstoreService.delete(key)

  // transform an uploaded image to the specified width x height
  def resizeImage(key: BlobKey, width: Int, height: Int): (BlobKey, String) = {
    // resize image to desired dimensions
    val image = imagesService.applyTransform(
      ImagesServiceFactory.makeResize(width, height, 0.5, 0.5),
      ImagesServiceFactory.makeImageFromBlob(key),
      OutputEncoding.JPEG)

    // delete the old blob
    blobstoreService.delete(key)

    // create a new blob file with mime-type "image/jpg"
    val file = fileService.createNewBlobFile("image/jpg");

    // open a channel to write to it and finalize
    val channel = fileService.openWriteChannel(file, true)
    channel.write(ByteBuffer.wrap(image.getImageData))
    channel.closeFinally

    // now read from the file using the Blobstore API; also return serving URL
    val rv = fileService.getBlobKey(file)
    rv -> imagesService.getServingUrl(rv)
  }

  @Inject var blobstoreService: BlobstoreService = _
  @Inject var fileService: FileService = _
  @Inject var imagesService: ImagesService = _
}