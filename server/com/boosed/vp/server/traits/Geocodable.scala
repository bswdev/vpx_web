package com.boosed.vp.shared.util
import com.boosed.vp.shared.exception.InvalidAddressException
import java.net.URL
import java.net.URLEncoder
import scala.xml.XML
import java.util.logging.Logger
import com.boosed.orm.gae.traits.Loggable

trait Geocodable
  extends Loggable {

  // maps api call
  val url = "http://maps.google.com/maps/api/geocode/xml?address=%s&sensor=false"

  def geocode(address: String): Pair[Double, Double] = {
    try {
      // don't look up blank addresses
      if (address.isEmpty) throw new Exception
      val source = XML.load(new URL(url format URLEncoder.encode(address, "UTF-8")))
      warning(source.toString)

      // return the coordinates
      (source \\ "location" \\ "lat").last.text.toDouble -> (source \\ "location" \\ "lng").last.text.toDouble
    } catch {
      case e =>
        warning("address: " concat address)
        throw new InvalidAddressException
    }
  }
}