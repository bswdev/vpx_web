package com.boosed.vp.server.traits
import com.boosed.orm.gae.service.impl.GenericServiceImpl
import com.googlecode.objectify.Key
import scala.collection.JavaConversions._

trait Countable[A] { self: GenericServiceImpl[A] =>

  def decrement(a: A)
  def increment(a: A)

  def updateCounts(decs: List[Key[A]], incs: List[Key[A]]) {

    // increments
    for ((k, v) <- dao.find(incs.filterNot(decs.contains): _*)) {
      increment(v)
      transact(_.persist(v))
    }

    //    dao.find(incs.filterNot(decs.contains): _*).values.foreach { c =>
    //      increment(c)
    //      transact(_.persist(c))
    //    }

    // decrements
    for ((k, v) <- dao.find(decs.filterNot(incs.contains): _*)) {
      decrement(v)
      transact(_.persist(v))
    }

    //    dao.find(decs.filterNot(incs.contains): _*).values.foreach { c =>
    //      decrement(c)
    //      transact(_.persist(c))
    //    }

    //    // alternate style
    //    val tx = (a: A) => transact(_.persist(a))
    //
    //    // increments
    //    dao.find(news.filterNot(olds.contains): _*).values.foreach(tx compose increment)
    //    //        { c =>
    //    //          c._2.total += 1
    //    //          transact(_.persist(c._2))
    //    //        }
    //
    //    // decrements
    //    dao.find(olds.filterNot(news.contains): _*).values.foreach(tx compose decrement)
    //    //        { c =>
    //    //          c._2.total -= 1
    //    //          transact(_.persist(c._2))
    //    //        }

    // can't handle multiple updates in a single transaction
    // increment (doesn't work)
    //transact(dao => dao.persist(dao.find(newcs.filterNot(oldcs.contains): _*).map(c => { c._2.total += 1; c._2 }).toList: _*))

    // decrement (doesn't work)
    //transact(dao => dao.persist(dao.find(oldcs.filterNot(newcs.contains): _*).map(c => { c._2.total -= 1; c._2 }).toList: _*))
  }
}