package com.boosed.vp.server.traits
import org.apache.commons.lang.{ StringEscapeUtils => SEU }

/**
 * Trait for xml escaping.
 */
trait Escapable {

  def escapeHtml(html: String): String = SEU.escapeHtml(html)
  
  def escapleXml(xml: String): String = SEU.escapeXml(xml)
}