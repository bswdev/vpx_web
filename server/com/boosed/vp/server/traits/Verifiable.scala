package com.boosed.vp.server.traits
import com.boosed.orm.gae.service.GenericService
import com.boosed.vp.shared.db.Account
import com.googlecode.objectify.Key
import com.boosed.vp.shared.exception._

/**
 * Trait for services which require an implicit account.
 */
trait Verifiable[A] { self: GenericService[A] =>

  // comparison check for whether account is authorized to access entity
  def owns(a: A)(implicit user: Key[Account]): Boolean
  
  // retrieve verified entity using long id
  def verified(id: Long)(implicit user: Key[Account]): A = get(id) match {
    case Some(x) => if (owns(x)) x else throw new UnauthorizedException
    case None => throw new NotFoundException
  }

  // retrieve verified entity using string id
  def verified(id: String)(implicit user: Key[Account]): A = get(id) match {
    case Some(x) => if (owns(x)) x else throw new UnauthorizedException
    case None => throw new NotFoundException
  }
}