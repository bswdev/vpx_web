package com.boosed.vp.server.traits

import com.googlecode.objectify.Key
import com.google.inject.Inject
import com.boosed.orm.gae.dao.crit.AncestorCriterion
import com.boosed.vp.server.service.CommentService
import com.boosed.vp.shared.db._
import scala.collection.mutable.Map

/**
 * Trait for components managing entities which can be commented upon.
 */
trait Commentable[A] extends Imageable[A] { //self: GenericService[A] =>

  // passthru for comment retrieval
  private implicit def passthru(c: Comment): Boolean = true
  
  // associate a comment with this entity
  def comment(subject: Key[A], vs: Map[String, String])(implicit user: Key[Account]): Comment =
    commentService.save(subject, vs)

  // delete all comments & images related to key
  def dropComments(key: Key[A]) = {
    val (cs, _)  = commentService.get(AncestorCriterion(key))
    cs.foreach(c => dropImage(c.image))
    commentService.drop(cs: _*)
  }

  // injections
  @Inject var commentService: CommentService = _
}