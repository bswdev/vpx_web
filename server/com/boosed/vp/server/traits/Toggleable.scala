package com.boosed.vp.server.traits
import com.boosed.orm.gae.service.GenericService
import com.googlecode.objectify.Key
import com.boosed.vp.shared.db.Account
import scala.collection.JavaConversions._

/**
 * Trait for services which manage entities with a boolean attribute.
 *
 * For an admin privileged call, invoke as: service.toggle(id)
 * i.e., both implicit parameters will be populated by the scope (auth & admin check)
 *
 * For an authorized privileged call, invoke as: service.toggle(id)(ensureAuthorized)
 * i.e., 2nd implicit parameter will use the default value of None (no admin check)
 */
trait Toggleable[A] { self: GenericService[A] =>

  // can be called naked for admin privileges; if just authorizing, PASS ONLY user for implicit args 
  // (admin will be NONE by default)
  def toggle(id: String)(implicit user: Key[Account], admin: Option[Boolean] = None): A =
    save(invert(id)).head._2

  // abstract implementation provided by service (invert the field)
  protected[Toggleable] def invert(id: String)(implicit user: Key[Account], admin: Option[Boolean]): A
}