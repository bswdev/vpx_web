package com.boosed.vp.server.traits
import com.boosed.vp.shared.db._
import com.googlecode.objectify.Key
import com.google.appengine.api.users.User
import com.google.appengine.api.blobstore.BlobKey

/**
 * Trait for facilitating implicit views (typically used in type ascriptions, e.g. val key: Key[Dish] = 13)
 */
trait Keyable {

  // id 2 key
  implicit def long2Dish(id: Long) = new Key(classOf[Dish], id)
  implicit def long2Kitchen(id: Long) = new Key(classOf[Kitchen], id)
  // not used
  //implicit def long2Key[T: ClassManifest](id: Long) = new Key(classManifest[T].erasure, id) 

  // entity 2 key
  implicit def blobKey2String(b: BlobKey): String = b.getKeyString
  implicit def category2Key(c: Category): Key[Category] = new Key(classOf[Category], c.id.longValue)
  implicit def comment2Key(c: Comment): Key[Comment] = new Key(classOf[Comment], c.id.longValue)
  implicit def cuisine2Key(c: Cuisine): Key[Cuisine] = new Key(classOf[Cuisine], c.id.longValue)
  implicit def dish2Key(d: Dish): Key[Dish] = new Key(classOf[Dish], d.id.longValue)
  implicit def kitchen2Key(k: Kitchen): Key[Kitchen] = new Key(classOf[Kitchen], k.id.longValue)
  implicit def string2BlobKey(s: String): BlobKey = new BlobKey(s)
  implicit def user2AccountKey(u: User): Key[Account] = Option(u) match {
    case Some(u) => new Key(classOf[Account], u.getUserId)
    case None => null
  }
  
  // not used
  //implicit def any2Key[T: ClassManifest](a: { val id: java.lang.Long }) = new Key(classManifest[T].erasure, a.id.longValue)

  //implicit val point2Location = (p: Point) => p.getLat() -> p.getLon()
  //implicit def stringToAccountKey = (user: String) => new Key(classOf[Account], user)
}