package com.boosed.vp.server.traits
import com.googlecode.objectify.Key
import com.google.inject.Inject
import com.boosed.vp.server.service.AccountService
import com.boosed.vp.shared.db.Account
import com.boosed.vp.shared.exception._
import com.google.appengine.api.users.UserService
import scala.collection.JavaConversions._
import com.google.appengine.api.users.User

/**
 * Trait for services which require accounts and authorization.
 */
trait Authorizable extends Keyable {

  // ensures user is logged in against the system
  implicit def authenticated: Key[Account] = Option(userService.getCurrentUser).getOrElse(throw new SessionTimeoutException)

  // like authenticated, but returns null instead of throwing error (i.e., suppresses exceptions)
  def accounted: Key[Account] = Option(userService.getCurrentUser).getOrElse(null)

  // like authenticated, but also checks if the user is enabled (i.e., not suspended)
  def enabled(implicit key: Key[Account]): Key[Account] =
    if (accountService.get(key).getOrElse {
      // create a new account & return
      val user = userService.getCurrentUser
      accountService.save(new Account(user.getUserId, user.getNickname, user.getEmail)).head._2
    }.enabled) key
    // not enabled
    else throw new UnauthorizedException

  // trick to specify implicit parameter via context bound, but using proper instead of first order type
  // (i.e., [A <: Account: Key] => Key[Account] vice [T: Key] => Key[T])

  // detects if user is an admin (context bounded to Key[A], namely Key[Account])
  // returns Option to permit context bounding in dependent services
  implicit def admin[A <: Account: Key]: Option[Boolean] = if (userService.isUserAdmin)
    Option(true) else throw new UnauthorizedException

  //injections
  @Inject var accountService: AccountService = _
  @Inject var userService: UserService = _
}