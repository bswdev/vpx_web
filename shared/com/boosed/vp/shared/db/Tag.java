package com.boosed.vp.shared.db;

import java.io.Serializable;

import javax.persistence.Id;

import com.boosed.vp.shared.db.enums.TagType;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Unindexed;

/**
 * Relation index entity for <code>Kitchen</code>s. Reduces key fetches on
 * <code>Account</code>. Represents a <code>Kitchen</code> a user has tagged.
 * 
 * @author dsumera
 */
@Cached(expirationSeconds=600)
@Unindexed
public class Tag implements Serializable {

	@Id
	public Long id;

	@Parent
	public Key<Kitchen> kitchen;

	@Indexed
	public Key<Account> account;

	@Indexed
	public TagType type;

	// sorted

	// @Indexed
	// public long time;

	// price alert fields

	@Indexed
	public long created = 0;

	public Tag() {
		// default no-arg constructor
		created = System.currentTimeMillis();
	}

	public Tag(Key<Kitchen> kitchen, Key<Account> account, TagType type) {
		this();
		this.kitchen = kitchen;
		this.account = account;
		this.type = type;
	}
}