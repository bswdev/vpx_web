package com.boosed.vp.shared.db.embed;

import java.io.Serializable;

import com.googlecode.objectify.annotation.NotSaved;
import com.googlecode.objectify.annotation.Unindexed;
import com.googlecode.objectify.condition.IfDefault;

/**
 * Image with url and blob key.
 * 
 * @author dsumera
 */
@Unindexed
public class Image implements Serializable {

	@NotSaved(IfDefault.class)
	public String key = "";

	@NotSaved(IfDefault.class)
	public String url = "";

	public Image() {
		// default no-arg constructor
	}
	
	public Image(String key, String url) {
		super();
		this.key = key;
		this.url = url;
	}
}