package com.boosed.vp.shared.db.embed;

import java.io.Serializable;

/**
 * Captures open/close times in minutes. A negative value indicates that there
 * is no associated time (e.g., closed).
 */
public class Hours implements Serializable {

	public int open;
	public int close;

	public Hours() {
		// default no-arg constructor
	}

	public Hours(int open, int close) {
		super();
		this.open = open;
		this.close = close;
	}
}