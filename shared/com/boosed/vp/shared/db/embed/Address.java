package com.boosed.vp.shared.db.embed;

import java.io.Serializable;

import com.googlecode.objectify.annotation.NotSaved;
import com.googlecode.objectify.annotation.Unindexed;
import com.googlecode.objectify.condition.IfDefault;

/**
 * Embeddable for address.
 * 
 * @author dsumera
 */
@Unindexed
public class Address implements Serializable {

	@NotSaved(IfDefault.class)
	public String street1 = "";
	
	@NotSaved(IfDefault.class)
	public String street2 = "";
	
	@NotSaved(IfDefault.class)
	public String city = "";
	
	@NotSaved(IfDefault.class)
	public String statoid = "";
	
	@NotSaved(IfDefault.class)
	public String country = "";
	
	@NotSaved(IfDefault.class)
	public String postal = "";

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if (!street1.equals(""))
			sb.append(street1).append(',');
		if (!street2.equals(""))
			sb.append(street2).append(',');
		if (!city.equals(""))
			sb.append(city).append(',');
		if (!statoid.equals(""))
			sb.append(statoid).append(',');
		if (!postal.equals(""))
			sb.append(postal).append(',');
		if (!country.equals(""))
			sb.append(country);
		return sb.toString();
	}
}