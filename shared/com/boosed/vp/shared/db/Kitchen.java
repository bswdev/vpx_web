package com.boosed.vp.shared.db;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.persistence.Embedded;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.boosed.vp.shared.db.embed.Address;
import com.boosed.vp.shared.db.embed.Hours;
import com.boosed.vp.shared.db.embed.Image;
import com.boosed.vp.shared.db.enums.Day;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.NotSaved;
import com.googlecode.objectify.annotation.Serialized;
import com.googlecode.objectify.annotation.Unindexed;
import com.googlecode.objectify.condition.IfDefault;
import com.googlecode.objectify.condition.IfNotNull;

/**
 * Kitchen item which creates <code>Food</code>.
 * 
 * @author dsumera
 */
// @Cached(expirationSeconds=600)
@Unindexed
public class Kitchen implements Serializable {

	@Id
	public Long id;

	public Key<Account> account = null;

	// lower is higher priority
	@Indexed
	public int priority = 100;

	// should be upper-case'd
	@Indexed
	public transient String search = "";

	// limit to 5 cuisines
	@Indexed
	public Set<Key<Cuisine>> cuisines = new HashSet<Key<Cuisine>>();

	@Indexed
	public transient List<String> location;

	/**
	 * all search queries use this field; not searchable unless it is paid for
	 * (listed != null)
	 */
	@Indexed(IfNotNull.class)
	public Long listed = System.currentTimeMillis();

	// for distance
	@NotSaved(IfDefault.class)
	public double latitude = 0;

	// for distance
	@NotSaved(IfDefault.class)
	public double longitude = 0;

	@NotSaved(IfDefault.class)
	public String name = "";

	@NotSaved(IfDefault.class)
	public String desc = "";

	@NotSaved(IfDefault.class)
	public String phone = "";

	@NotSaved(IfDefault.class)
	public String homepage = "";

	@NotSaved(IfDefault.class)
	public String facebook = "";

	@NotSaved(IfDefault.class)
	public String alternate = "";

	@NotSaved(IfDefault.class)
	public int recommends = 0;

	@Embedded
	public Address address = new Address();

	@Embedded
	public Image image = new Image();

	@Serialized
	public Map<Day, Hours> schedule = new TreeMap<Day, Hours>();

	// how far from search location
	@Transient
	public double distance = 0;

	// list of cuisines
	@Transient
	public String tags = "";

	public Kitchen() {
		// init the schedule
		for (Day day : Day.values())
			schedule.put(day, new Hours(0, 0));
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public double getDistance() {
		return distance;
	}

	public String getDesc() {
		return desc;
	}

	public String getTags() {
		return tags;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if (!address.street1.equals(""))
			sb.append(address.street1).append("<br/>");
		if (!address.street2.equals(""))
			sb.append(address.street2).append("<br/>");
		if (!address.city.equals(""))
			sb.append(address.city).append(", ");
		if (!address.statoid.equals(""))
			sb.append(address.statoid).append(" ");
		if (!address.postal.equals(""))
			sb.append(address.postal).append(" ");
		if (!address.country.equals(""))
			sb.append(address.country);
		if (!phone.equals(""))
			sb.append("<br/>").append(phone);
		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Kitchen other = (Kitchen) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}