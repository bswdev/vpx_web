package com.boosed.vp.shared.db;

import java.io.Serializable;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Embedded;
import javax.persistence.Id;

import com.boosed.vp.shared.db.embed.Image;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.NotSaved;
import com.googlecode.objectify.annotation.Serialized;
import com.googlecode.objectify.annotation.Unindexed;
import com.googlecode.objectify.condition.IfDefault;

/**
 * Something a <code>Kitchen</code> whips up.
 * 
 * @author dsumera
 */
//@Cached(expirationSeconds=600)
@Unindexed
public class Dish implements Serializable {

	@Id
	public Long id;
	
	public Key<Kitchen> kitchen = null;
	
	// limit to 5 (category -> kitchen, duplicates relation above)
	@Indexed
	public Set<Key<Category>> categories = new HashSet<Key<Category>>();
	
	@Indexed
	public String name = "";
	
	@NotSaved(IfDefault.class)
	public String desc = "";
	
	@NotSaved(IfDefault.class)
	public boolean visible = true;
	
	@NotSaved(IfDefault.class)
	public int recommends = 0;
	
	// TODO change this to a serialized rate map
	//@NotSaved(IfDefault.class)
	//public double price = 0.0;
	@Serialized
	public Map<String, String> rates = new LinkedHashMap<String, String>();
	
	@Embedded
	public Image image = new Image();
}