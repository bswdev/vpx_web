package com.boosed.vp.shared.db;

import java.io.Serializable;

import javax.persistence.Id;

import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.NotSaved;
import com.googlecode.objectify.annotation.Unindexed;
import com.googlecode.objectify.condition.IfDefault;

/**
 * Type of <code>Kitchen</code>. Can have multiple <code>Cuisine</code>s.
 * 
 * @author dsumera
 */
@Cached(expirationSeconds=600)
@Unindexed
public class Cuisine implements Serializable {

	@Id
	public Long id;

	@Indexed
	@NotSaved(IfDefault.class)
	public String name = "";

	@NotSaved(IfDefault.class)
	public int total = 0;

	public Cuisine() {
		// default no-arg constructor
	}

	public Cuisine(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cuisine other = (Cuisine) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public String label() {
		return name + " (" + total + ")";
	}

	@Override
	public String toString() {
		return name;
	}
}