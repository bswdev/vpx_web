package com.boosed.vp.shared.db;

import java.io.Serializable;

import javax.persistence.Embedded;
import javax.persistence.Id;
import javax.persistence.PrePersist;

import com.boosed.vp.shared.db.embed.Image;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.NotSaved;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Unindexed;
import com.googlecode.objectify.condition.IfDefault;

@Unindexed
public class Comment implements Serializable {

	@Id
	public Long id;

	// subject of comment
	@Parent
	public Key<?> subject;
	
	@Indexed
	public Key<Account> account;
	
	@Indexed
	boolean isComment = false;
	
	@Indexed
	boolean isImage = false;
	
	@Embedded
	public Image image = new Image();
	
	// sorted

	@Indexed
	public long created = 0;

	// fields
	
	@NotSaved(IfDefault.class)
	public String handle = "anonymous";
	
	@NotSaved(IfDefault.class)
	public String comment = "";
	
	@NotSaved(IfDefault.class)
	public boolean recommend = false;
	
	public Comment() {
		// default no-arg constructor
		created = System.currentTimeMillis();
	}

	public Comment(Key<?> subject, Key<Account> account) {
		this();
		this.subject = subject;
		this.account = account;
	}
	
	@SuppressWarnings("unused")
	@PrePersist
	private void prePersist() {
		// update time
		created = System.currentTimeMillis();
		
		// set boolean indexes
		isComment = !comment.equals("");
		isImage = !image.key.equals("");
	}

	//public void setHandle(String handle) {
	//	this.handle = handle.isEmpty() ? "anonymous" : handle;
	//}
}