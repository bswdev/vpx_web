package com.boosed.vp.shared.db;

import java.io.Serializable;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.NotSaved;
import com.googlecode.objectify.annotation.Unindexed;
import com.googlecode.objectify.condition.IfDefault;

/**
 * Grouping of <code>Dish</code>. Can have multiple <code>Category</code>s.
 * 
 * @author dsumera
 */
//@Cached(expirationSeconds=600)
@Unindexed
public class Category implements Serializable {

	@Id
	public Long id;

	@Indexed
	public Key<Kitchen> kitchen = null;
	
	@Indexed
	@NotSaved(IfDefault.class)
	public String name = "";
	
	// grouping of common dishes (may come with salad, roll, side, etc.)
	@NotSaved(IfDefault.class)
	public String desc = "";

	@NotSaved(IfDefault.class)
	public int total = 0;

	public Category() {
		// default no-arg constructor
	}

	public Category(String name) {
		this();
		this.name = name;
	}
	
	public Category(String name, Key<Kitchen> kitchen) {
		this(name);
		this.kitchen = kitchen;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Category other = (Category) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public String label() {
		return name + " (" + total + ")";
	}
}