package com.boosed.vp.shared.db;

import java.io.Serializable;

public class Tuple<A, B> implements Serializable {

	public A a;
	public B b;
	
	public Tuple() {
		// default no-arg constructor
	}
	
	public Tuple(A a, B b) {
		this.a = a;
		this.b = b;
	}
}
