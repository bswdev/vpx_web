package com.boosed.vp.shared.db.enums;

/**
 * Type of <code>Tag</code> used to annotate a <code>Kitchen</code> wrt an
 * <code>Account</code>.
 * 
 * @author dsumera
 */
public enum TagType {

	MANAGED, FAVORITE
}
