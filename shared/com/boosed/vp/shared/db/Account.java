package com.boosed.vp.shared.db;

import java.io.Serializable;

import javax.persistence.Id;

import com.googlecode.objectify.annotation.NotSaved;
import com.googlecode.objectify.condition.IfDefault;

/**
 * User account
 * 
 * @author dsumera
 */
public class Account implements Serializable {

	@Id
	public String id;

	// display name
	@NotSaved(IfDefault.class)
	public String name = "";

	// email (for user service)
	@NotSaved(IfDefault.class)
	public String email = "";

	@SuppressWarnings("unused")
	@NotSaved(IfDefault.class)
	private transient String gmail = "";

	// creation date
	public Long created;

	/** check whether the account privileges revoked */
	@NotSaved(IfDefault.class)
	public boolean enabled = true;
	
	public Account() {
		// default no-arg constructor
		created = System.currentTimeMillis();
	}

	public Account(String id, String name, String email) {
		this();
		this.id = id;
		this.name = name;
		this.email = email;

		// the user's original email
		gmail = email;
	}
}