package com.boosed.vp.shared.exception;

/**
 * User has not provided proper data to perform update.
 * 
 * @author dsumera
 */
public class InvalidIdException extends RemoteServiceFailureException {

	public InvalidIdException() {
		// default no-arg constructor
	}
	
	public InvalidIdException(String msg) {
		super(msg);
	}
}