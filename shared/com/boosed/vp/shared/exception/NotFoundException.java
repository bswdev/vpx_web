package com.boosed.vp.shared.exception;

/**
 * Entity doesn't exist or user is not owner.
 * 
 * @author dsumera
 */
public class NotFoundException extends RemoteServiceFailureException {

	public NotFoundException() {
		// default no-arg constructor
	}
	
	public NotFoundException(String msg) {
		super(msg);
	}
}