package com.boosed.vp.shared.exception;

/**
 * Generic base exception for all system generated errors.
 * 
 * @author dsumera
 */
public class RemoteServiceFailureException extends Exception {

	public RemoteServiceFailureException() {
		// default no-arg constructor
	}

	public RemoteServiceFailureException(String msg) {
		super(msg);
	}

	public RemoteServiceFailureException(Throwable throwable) {
		super(throwable);
	}
}
