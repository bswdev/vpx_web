package com.boosed.vp.shared.exception;

/**
 * User trying to delete a non-empty collection.
 * 
 * @author dsumera
 */
public class NonEmptyException extends RemoteServiceFailureException {

	public NonEmptyException() {
		// default no-arg constructor
	}
	
	public NonEmptyException(String msg) {
		super(msg);
	}
}