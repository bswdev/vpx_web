package com.boosed.vp.shared.exception;

/**
 * User provided an un-geocodable address.
 * 
 * @author dsumera
 */
public class InvalidAddressException extends RemoteServiceFailureException {

	public InvalidAddressException() {
		// default no-arg constructor
	}
}