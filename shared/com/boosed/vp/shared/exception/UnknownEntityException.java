package com.boosed.vp.shared.exception;

/**
 * User provided an invalid entity "kind."
 * 
 * @author dsumera
 */
public class UnknownEntityException extends RemoteServiceFailureException {

	public UnknownEntityException() {
		// default no-arg constructor
	}
	
	public UnknownEntityException(String msg) {
		super(msg);
	}
}