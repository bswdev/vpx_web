package com.boosed.vp.shared.exception;

/**
 * User has not provided proper data to perform update.
 * 
 * @author dsumera
 */
public class EntityUpdateException extends RemoteServiceFailureException {

	public EntityUpdateException() {
		// default no-arg constructor
	}
	
	public EntityUpdateException(String msg) {
		super(msg);
	}
}