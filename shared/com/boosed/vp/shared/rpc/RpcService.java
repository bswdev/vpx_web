package com.boosed.vp.shared.rpc;

import java.util.List;
import java.util.Map;

import com.boosed.vp.shared.db.Account;
import com.boosed.vp.shared.db.Category;
import com.boosed.vp.shared.db.Comment;
import com.boosed.vp.shared.db.Cuisine;
import com.boosed.vp.shared.db.Dish;
import com.boosed.vp.shared.db.Kitchen;
import com.boosed.vp.shared.db.Tuple;
import com.boosed.vp.shared.db.embed.Hours;
import com.boosed.vp.shared.db.enums.Day;
import com.boosed.vp.shared.db.enums.TagType;
import com.boosed.vp.shared.exception.RemoteServiceFailureException;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("rpc")
public interface RpcService extends RemoteService {

	/**
	 * Client must invoke to engage in authenticated functionality.
	 * 
	 * @return
	 */
	String initialize();

	/**
	 * Creates a login url for user to authenticate.
	 * 
	 * @param url
	 * @return
	 * @throws RemoteServiceFailureException
	 */
	String createLoginUrl(String url) throws RemoteServiceFailureException;

	/**
	 * Retrieve an upload action url to submit image.
	 * 
	 * @param url
	 * @return
	 */
	String createUploadUrl(String url) throws RemoteServiceFailureException;

	/**
	 * Retrieve user's <code>Account</code>.
	 * 
	 * @return
	 * @throws RemoteServiceFailureException
	 */
	Account loadAccount() throws RemoteServiceFailureException;

	/**
	 * Retrieve comment for an item published by a user.
	 * 
	 * @param comment
	 * @return
	 * @throws RemoteServiceFailureException
	 */
	Comment loadComment(Tuple<String, Long> comment) throws RemoteServiceFailureException;

	/**
	 * Retrieve list of <code>Cuisine</code>s.
	 * 
	 * @return
	 */
	List<Cuisine> loadCuisines();

	/**
	 * Load a <code>Kitchen</code> by its id.
	 * 
	 * @param kitchen
	 * @return
	 * @throws RemoteServiceFailureException
	 */
	Kitchen loadKitchen(long kitchen) throws RemoteServiceFailureException;

	/**
	 * Load <code>Kitchen</code>s which belong to user.
	 * 
	 * @param type
	 * @param cursor
	 *            is null if represents beginning of list
	 * @return
	 * @throws RemoteServiceFailureException
	 */
	Tuple<List<Kitchen>, String> loadKitchens(TagType type, String cursor) throws RemoteServiceFailureException;

	/**
	 * Load <code>Categories</code> by a given <code>Kitchen</code> id.
	 * 
	 * @param kitchen
	 * @return
	 * @throws RemoteServiceFailureException
	 */
	List<Category> loadCategories(long kitchen, boolean isMenu) throws RemoteServiceFailureException;

	/**
	 * Load <code>Dishes</code> provided the given <code>Category</code> id.
	 * 
	 * @param category
	 * @return
	 */
	List<Dish> loadDishes(long category, boolean isMenu);

	/**
	 * Remove by kind and id.
	 * 
	 * @param kind
	 * @param id
	 * @throws RemoteServiceFailureException
	 */
	void remove(String kind, long id) throws RemoteServiceFailureException;

	/**
	 * Set the schedule for a <code>Kitchen</code>.
	 * 
	 * @param kitchen
	 * @param schedule
	 * @throws RemoteServiceFailureException
	 */
	void schedule(long kitchen, Map<Day, Hours> schedule) throws RemoteServiceFailureException;

	/**
	 * Find all <code>Kitchen</code>s with given search criteria
	 * 
	 * @param cuisines
	 * @param search
	 * @param location
	 * @param offset
	 * @param limit
	 * @return
	 */
	List<Kitchen> search(List<Long> cuisines, String search, List<Double> location, int offset, int limit);

	/**
	 * Store/update entity
	 * 
	 * @param values
	 * @throws RemoteServiceFailureException
	 */
	void store(Map<String, String> values) throws RemoteServiceFailureException;

	/**
	 * Toggles boolean state of an entity.
	 * 
	 * @param values
	 * @throws RemoteServiceFailureException
	 */
	boolean toggle(Tuple<String, String> values) throws RemoteServiceFailureException;
}