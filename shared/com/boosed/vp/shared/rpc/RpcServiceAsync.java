package com.boosed.vp.shared.rpc;

import java.util.List;
import java.util.Map;

import com.boosed.vp.shared.db.Account;
import com.boosed.vp.shared.db.Category;
import com.boosed.vp.shared.db.Comment;
import com.boosed.vp.shared.db.Cuisine;
import com.boosed.vp.shared.db.Dish;
import com.boosed.vp.shared.db.Kitchen;
import com.boosed.vp.shared.db.Tuple;
import com.boosed.vp.shared.db.embed.Hours;
import com.boosed.vp.shared.db.enums.Day;
import com.boosed.vp.shared.db.enums.TagType;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The client side stub for the RPC service.
 */
public interface RpcServiceAsync {

	/**
	 * Client must invoke to engage in authenticated functionality.
	 */
	void initialize(AsyncCallback<String> callback);

	/**
	 * Creates a login url for user to authenticate.
	 * 
	 * @param url
	 * @throws RemoteServiceFailureException
	 */
	void createLoginUrl(String url, AsyncCallback<String> callback);

	/**
	 * Retrieve an upload action url to submit image.
	 * 
	 * @param url
	 */
	void createUploadUrl(String url, AsyncCallback<String> callback);

	/**
	 * Retrieve user's <code>Account</code>.
	 * 
	 * @throws RemoteServiceFailureException
	 */
	void loadAccount(AsyncCallback<Account> callback);

	/**
	 * Retrieve comment for an item published by a user.
	 * 
	 * @param comment
	 * @throws RemoteServiceFailureException
	 */
	void loadComment(Tuple<String, Long> comment, AsyncCallback<Comment> callback);

	/**
	 * Retrieve list of <code>Cuisine</code>s.
	 */
	void loadCuisines(AsyncCallback<List<Cuisine>> callback);

	/**
	 * Load a <code>Kitchen</code> by its id.
	 * 
	 * @param kitchen
	 * @throws RemoteServiceFailureException
	 */
	void loadKitchen(long kitchen, AsyncCallback<Kitchen> callback);

	/**
	 * Load <code>Kitchen</code>s which belong to user.
	 * 
	 * @param type
	 * @param cursor
	 *            is null if represents beginning of list
	 * @throws RemoteServiceFailureException
	 */
	void loadKitchens(TagType type, String cursor, AsyncCallback<Tuple<List<Kitchen>, String>> callback);

	/**
	 * Load <code>Categories</code> by a given <code>Kitchen</code> id.
	 * 
	 * @param kitchen
	 * @throws RemoteServiceFailureException
	 */
	void loadCategories(long kitchen, boolean isMenu, AsyncCallback<List<Category>> callback);

	/**
	 * Load <code>Dishes</code> provided the given <code>Category</code> id.
	 * 
	 * @param category
	 */
	void loadDishes(long category, boolean isMenu, AsyncCallback<List<Dish>> callback);

	/**
	 * Remove by kind and id.
	 * 
	 * @param kind
	 * @param id
	 * @throws RemoteServiceFailureException
	 */
	void remove(String kind, long id, AsyncCallback<Void> callback);

	/**
	 * Set the schedule for a <code>Kitchen</code>.
	 * 
	 * @param kitchen
	 * @param schedule
	 * @throws RemoteServiceFailureException
	 */
	void schedule(long kitchen, Map<Day, Hours> schedule, AsyncCallback<Void> callback);

	/**
	 * Find all <code>Kitchen</code>s with given search criteria
	 * 
	 * @param cuisines
	 * @param search
	 * @param location
	 * @param offset
	 * @param limit
	 */
	void search(List<Long> cuisines, String search, List<Double> location, int offset, int limit, AsyncCallback<List<Kitchen>> callback);

	/**
	 * Store/update entity
	 * 
	 * @param values
	 * @throws RemoteServiceFailureException
	 */
	void store(Map<String, String> values, AsyncCallback<Void> callback);

	/**
	 * Toggles boolean state of an entity.
	 * 
	 * @param values
	 * @throws RemoteServiceFailureException
	 */
	void toggle(Tuple<String, String> values, AsyncCallback<Boolean> callback);
}